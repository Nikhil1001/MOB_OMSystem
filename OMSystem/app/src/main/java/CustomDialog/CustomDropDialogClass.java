package CustomDialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.om.omsystem.R;

import java.util.ArrayList;

import adapter.DropDownAdapter;
import adapter.DropDownAdapter1;
import entity.DDLEntity;

/**
 * Created by admin on 3/23/2018.
 */

public class CustomDropDialogClass extends Dialog {


    public long ID;
    public String Name, Header;
    private ListView lsItems;
    private EditText txtSearch;
    private Activity C;
    private ArrayList<DDLEntity> lstData;
    private OnDailogDimmis onDailogDimmis;
    private DropDownAdapter1 ad_PayType;


    public CustomDropDialogClass(Activity ac) {
        super(ac);
        this.C = ac;
    }

    public CustomDropDialogClass(Activity ac, String Header, ArrayList<DDLEntity> lstData, OnDailogDimmis onDailogDimmis) {
        super(ac);
        this.C = ac;
        this.lstData = lstData;
        this.onDailogDimmis = onDailogDimmis;
        this.Header = Header;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custome_dropdown);
        setTitle(Header);
        this.getWindow().setTitleColor(C.getResources().getColor(R.color.blue));
        lsItems = (ListView) findViewById(R.id.lst_custome_dropdown);
        txtSearch = (EditText) findViewById(R.id.txt_custome_search);


        ad_PayType = new DropDownAdapter1(getContext(), lstData);
        lsItems.setAdapter(ad_PayType);
        lsItems.setClickable(true);
        lsItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                ID = ad_PayType.getItemId(position);
                // Name = ad_PayType.getItem(position).getName();

                dismiss();
                onDailogDimmis.OnIdSelect(ad_PayType.getItemId(position), ad_PayType.getItem(position));
                // Object o = prestListView.getItemAtPosition(position);
                // String str=(String)o;//As you are using Default String Adapter
                //Toast.makeText(getApplicationContext(),str,Toast.LENGTH_SHORT).show();
            }
        });


        txtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                ad_PayType.getFilter().filter(cs.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //  Toast.makeText(C.getApplicationContext(), "before text change", Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //  Toast.makeText(C.getApplicationContext(), "after text change", Toast.LENGTH_LONG).show();
            }
        });

    }


    public interface OnDailogDimmis {
        void OnIdSelect(long ID, String Name);
    }


}
