package DBClasses;

import android.content.ContentValues;

import Web.WebSevice;
import Web.WebSevice.FuctionName;

public class DBMState extends Entity {

    CommonClass cc = new CommonClass();

    private String[] filedNames = {"PkSrNo", "StateNo", "StateName"};

    private static String tableName = "MState";

    public static String CreateTable_MState = "CREATE TABLE IF NOT EXISTS "
            + tableName
            + "(PkSrNo integer primary key autoincrement "
            + ",StateNo INTEGER,StateName VARCHAR)";

    private boolean AddMState(String Values) {
        boolean flag = false;


        String str;

        str = "\n";
        String[] strLine = Values.split(str);
        String[] strData;
        for (int i = 0; i < strLine.length; i++) {

            String strs = "|||";
            strData = strLine[i].replace("|||", ",").split(",");
            DBMState mnot = new DBMState();

            mnot.setmStateNo(Long.parseLong(strData[0]));
            mnot.setmStateName(strData[1]);
            long ID = cc.createEntity(mnot);
            flag = true;

        }

        return flag;

    }

    @Override
    public ContentValues values() {
        ContentValues values = new ContentValues();
        values.put(filedNames[1], mStateNo);
        values.put(filedNames[2], mStateName);

        return values;
    }

    public void SyncState() {
        String StrDate = "";
        long SCount = cc.ReturnLong("Select Max(PkSrNo)  from " + tableName);
        if (SCount == 0) {
            StrDate = WebSevice.GetServiceData("" + FuctionName.State);
        }
        // AddComplaintType
        if (!StrDate.equalsIgnoreCase("")) {

            AddMState(StrDate);
        }
    }

    @Override
    public String getTableName() {

        return tableName;
    }

    private long mPkSrNo;
    private long mStateNo;
    private String mStateName;

    public long getmPkSrNo() {
        return mPkSrNo;
    }

    public void setmPkSrNo(long mPkSrNo) {
        this.mPkSrNo = mPkSrNo;
    }

    public long getmStateNo() {
        return mStateNo;
    }

    public void setmStateNo(long mStateNo) {
        this.mStateNo = mStateNo;
    }

    public String getmStateName() {
        return mStateName;
    }

    public void setmStateName(String mStateName) {
        this.mStateName = mStateName;
    }

}
