package DBClasses;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import entity.UCSaleBills;

/**
 * Created by admin on 9/28/2017.
 */

public class DBTVoucherRefDetails {

    CommonClass cc=new CommonClass();

    private String[] filedNames = {"PkSrNo","PkRefTrnNo","FkVoucherNo","LedgerNo","Refno","TypeofRef","Amount","DiscAmount","BalAmount","PayDate"
            ,"PayType","CardNo","CheqDate","Remark"};
    public static String tableName = "TVoucherRefDetails";

    public static String CreateTable_TVoucherRefDetails = "CREATE TABLE IF NOT EXISTS "
            + tableName
            + "(PkSrNo integer primary key autoincrement "
            + ",PkRefTrnNo INTEGER,FkvoucherNo INTEGER,LedgerNo INTEGER,Refno INTEGER,TypeofRef INTEGER,Amount Double"
            + ",DiscAmount Double,BalAmount Double,PayDate VARCHAR,PayType INTEGER,CardNo VARCHAR,CheqDate VARCHAR,Remark VARCHAR )";



    public ContentValues AddTVoucherRefDetails(UCSaleBills.UCTVoucherRefDetails tvr) {
        ContentValues values = new ContentValues();

        values.put(filedNames[1], tvr.PkreftrnNo);
        values.put(filedNames[2], tvr.FkvoucherNo);
        values.put(filedNames[3], tvr.LedgerNo);
        values.put(filedNames[4], tvr.Refno);
        values.put(filedNames[5], tvr.TypeofRef);
        values.put(filedNames[6], tvr.Amount);
        values.put(filedNames[7], tvr.DiscAmount);
        values.put(filedNames[8], tvr.Balamount);
        values.put(filedNames[9], tvr.PayDate);
        values.put(filedNames[10], tvr.PayType);
        values.put(filedNames[11], tvr.CardNo);
        values.put(filedNames[12], tvr.CheqDate);
        values.put(filedNames[13], tvr.Remark);

        return values;
    }


    public List<UCSaleBills.UCTVoucherRefDetails> GetTVoucherRef(long ID) {
        String str = " Select PkSrNo,PkRefTrnNo,FkVoucherNo,LedgerNo,Refno,TypeofRef,Amount,DiscAmount,BalAmount,PayDate " +
                ",PayType,CardNo,CheqDate,Remark " +
                " From TVoucherRefDetails" +
                " Where Refno=" + ID + " ";

        Cursor cursor = cc.GetDataView(str);
        if (cursor.getCount() == 0)
            return null;

        List<UCSaleBills.UCTVoucherRefDetails> lstRef= new ArrayList<>();
        while (cursor.moveToNext()) {
            UCSaleBills.UCTVoucherRefDetails tvch = new UCSaleBills().new UCTVoucherRefDetails();

            tvch.PkreftrnNo = cursor.getLong(1);
            tvch.FkvoucherNo= cursor.getLong(2);
            tvch.LedgerNo = cursor.getLong(3);
            tvch.Refno = cursor.getLong(4);
            tvch.TypeofRef = cursor.getLong(5);
            tvch.Amount = cursor.getDouble(6);
            tvch.DiscAmount = cursor.getLong(7);
            tvch.Balamount = cursor.getDouble(8);
            tvch.PayDate = cursor.getString(9);
            tvch.PayType= cursor.getString(10);
            tvch.CardNo = cursor.getString(11);
            tvch.CheqDate = cursor.getString(12);
            tvch.Remark = cursor.getString(13);
            lstRef.add(tvch);
        }

        return lstRef;
    }

    public UCSaleBills.UCTVoucherRefDetails GetCollectionData(long ID) {
        String str = " Select TR.PkSrNo,TR.PkRefTrnNo,TE.PkVoucherNo,TR.LedgerNo,TR.Refno,TR.TypeofRef,TR.Amount,TR.DiscAmount,TR.BalAmount,TR.PayDate " +
                ",TR.PayType,TR.CardNo,TR.CheqDate,TR.Remark " +
                " From TVoucherRefDetails AS TR Inner join TVoucherEntry AS TE ON TE.PkSrNo=TR.RefNo " +
                " Where TR.PkSrNo=" + ID + " ";

        Cursor cursor = cc.GetDataView(str);
        if (cursor.getCount() == 0)
            return null;

        UCSaleBills.UCTVoucherRefDetails tvch=null;
        while (cursor.moveToNext()) {
            tvch=new UCSaleBills(). new UCTVoucherRefDetails();

            tvch.PkSrNo= cursor.getLong(0);
            tvch.PkreftrnNo = cursor.getLong(1);
            tvch.FkvoucherNo= cursor.getLong(2);
            tvch.LedgerNo = cursor.getLong(3);
            tvch.Refno = cursor.getLong(4);
            tvch.TypeofRef = cursor.getLong(5);
            tvch.Amount = cursor.getDouble(6);
            tvch.DiscAmount = cursor.getLong(7);
            tvch.Balamount = cursor.getDouble(8);
            tvch.PayDate = cursor.getString(9);
            tvch.PayType= cursor.getString(10);
            tvch.CardNo = cursor.getString(11);
            tvch.CheqDate = cursor.getString(12);
            tvch.Remark = cursor.getString(13);
        }

        return tvch;
    }


    }
