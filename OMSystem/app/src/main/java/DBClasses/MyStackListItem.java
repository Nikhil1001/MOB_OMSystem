package DBClasses;

import android.os.Bundle;

public class MyStackListItem {
	int FrgmtNo;
	Bundle bundle;
	
	public MyStackListItem(int frgmtNo, Bundle bundle) {
		super();
		FrgmtNo = frgmtNo;
		this.bundle = bundle;
	}
	
	public int getFrgmtNo() {
		return FrgmtNo;
	}
	public void setFrgmtNo(int Frgmtno) {
		FrgmtNo = Frgmtno;
	}
	public Bundle getBundle() {
		return bundle;
	}
	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
	}
}
