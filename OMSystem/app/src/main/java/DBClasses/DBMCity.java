package DBClasses;

import android.content.ContentValues;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Web.WebSevice;
import Web.WebSevice.FuctionName;
import entity.UCCity;

public class DBMCity  {

    CommonClass cc = new CommonClass();

    private String[] filedNames = {"PkSrNo", "CityNo", "CityName"};

    private static String tableName = "MCity";

    public static String CreateTable_MCity = "CREATE TABLE IF NOT EXISTS "
            + tableName
            + "(PkSrNo integer primary key autoincrement "
            + ",CityNo INTEGER,CityName VARCHAR "
            + " )";


    public void SaveCity(List<UCCity> items) {

        for (int i = 0; i < items.size(); i++) {
            UCCity item=items.get(i);
            ContentValues values = new ContentValues();
            values.put(filedNames[1], item.CityNo);
            values.put(filedNames[2], item.CityName);

            long ID = cc.createEntity(tableName, values);
            if (ID == -1) {
                Log.e("", "Items Not Save :" + item.CityNo);
                throw new RuntimeException("City Not Save");
            }
        }
    }

    private long mPkSrNo;
    private long mCityNo;
    private String mCityName;

    public long getmPkSrNo() {
        return mPkSrNo;
    }

    public void setmPkSrNo(long mPkSrNo) {
        this.mPkSrNo = mPkSrNo;
    }

    public long getmCityNo() {
        return mCityNo;
    }

    public void setmCityNo(long mCityNo) {
        this.mCityNo = mCityNo;
    }

    public String getmCityName() {
        return mCityName;
    }

    public void setmCityName(String mCityName) {
        this.mCityName = mCityName;
    }
}
