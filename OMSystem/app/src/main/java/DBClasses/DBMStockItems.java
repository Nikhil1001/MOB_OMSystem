package DBClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import entity.Item;
import entity.UCStockItems;


/**
 * Created by admin on 3/2/2017.
 */

public class DBMStockItems {


    private static String tableName = "MStockItems";
    public static String CreateTable_MStockItems = "CREATE TABLE IF NOT EXISTS "
            + tableName
            + "(PkSrNo integer primary key autoincrement "
            + ",ItemNo integer "
            + ",BrandName VARCHAR,ItemName VARCHAR,LangFullDesc VARCHAR "
            + ",MRP double,Rate double,Uom VARCHAR,Barcode VARCHAR"
            + ",ItemNameDisp VARCHAR,TaxRate double,IGSTPer double,CGSTPer double,SGSTPer double,CessPer double )";
    CommonClass cc = new CommonClass();
    private String[] filedNames = {"PkSrNo", "ItemNo", "BrandName", "ItemName", "LangFullDesc", "MRP", "Rate", "Uom", "Barcode", "ItemNameDisp", "TaxRate", "IGSTPer", "CGSTPer", "SGSTPer", "CessPer"};

    public void Save_MStockItems(List<UCStockItems> dtoItems) {
        for (int i = 0; i < dtoItems.size(); i++) {

            UCStockItems items = dtoItems.get(i);
            ContentValues values = new ContentValues();
            values.put(filedNames[1], items.ItemNo);
            values.put(filedNames[2], items.BrandName);
            values.put(filedNames[3], items.ItemName);
            values.put(filedNames[4], items.LangFullDesc);
            values.put(filedNames[5], items.MRP);
            values.put(filedNames[6], items.Rate);
            values.put(filedNames[7], items.Uom);
            values.put(filedNames[8], items.Barcode);
            values.put(filedNames[9], items.ItemNameDisp);
            values.put(filedNames[10], items.TaxRate);
            values.put(filedNames[11], items.IGSTPer);
            values.put(filedNames[12], items.CGSTPer);
            values.put(filedNames[13], items.SGSTPer);
            values.put(filedNames[14], items.CessPer);

            long ID = cc.createEntity(tableName, values);
            if (ID == -1) {
                Log.e("", "Items Not Save :" + items.ItemNo);
                throw new RuntimeException("Items Not Save");
            }
        }
    }

    public List<Item> getItemListBySearch(String searchText) {
        Cursor cursor = cc.GetDataView("Select ItemNo,ItemName,MRP,Barcode,PkSrno,BrandName,UOM,ItemNameDisp,TaxRate, " +
                "IGSTPer,CGSTPer,SGSTPer,CessPer,Rate,PkSrNo  from MStockItems Where ItemNameDisp like '%" + searchText + "%' OR Barcode like '%" + searchText + "%' ");
        if (cursor.getCount() == 0)
            return null;
        List<Item> productList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Item item = new Item();
            item.setItemNo(cursor.getLong(0));
            item.setItemName(cursor.getString(1));
            item.setMrp(cursor.getDouble(2));
            item.setBarcode(cursor.getString(3));
            item.setPkSrNo(cursor.getLong(4));
            item.setBrandName(cursor.getString(5));
            item.setUOM(cursor.getString(6));
            item.setItemNameDisp(cursor.getString(7));
            item.setTaxRate(cursor.getDouble(8));
            item.setIGSTPer(cursor.getDouble(9));
            item.setCGSTPer(cursor.getDouble(10));
            item.setSGSTPer(cursor.getDouble(11));
            item.setCessPer(cursor.getDouble(12));
            item.setRate(cursor.getDouble(13));
            item.setPkSrNo(cursor.getLong(14));

            productList.add(item);
        }
        return productList;
    }


    public Item BarcodeSerach(String searchText) {
        Cursor cursor = cc.GetDataView("Select ItemNo,ItemName,MRP,Barcode,PkSrno,BrandName,UOM,ItemNameDisp,TaxRate, " +
                "IGSTPer,CGSTPer,SGSTPer,CessPer,Rate,PkSrNo  from MStockItems Where Barcode = '" + searchText + "' ");
        if (cursor.getCount() == 0)
            throw new RuntimeException("Barcode Not Found..");

        Item item =null;
        if(cursor.moveToNext()) {
            item = new Item();
            item.setItemNo(cursor.getLong(0));
            item.setItemName(cursor.getString(1));
            item.setMrp(cursor.getDouble(2));
            item.setBarcode(cursor.getString(3));
            item.setPkSrNo(cursor.getLong(4));
            item.setBrandName(cursor.getString(5));
            item.setUOM(cursor.getString(6));
            item.setItemNameDisp(cursor.getString(7));
            item.setTaxRate(cursor.getDouble(8));
            item.setIGSTPer(cursor.getDouble(9));
            item.setCGSTPer(cursor.getDouble(10));
            item.setSGSTPer(cursor.getDouble(11));
            item.setCessPer(cursor.getDouble(12));
            item.setRate(cursor.getDouble(13));
            item.setPkSrNo(cursor.getLong(14));
        }

        if(item==null)
            throw new RuntimeException("Barcode Not Found..");
        return item;
    }


}