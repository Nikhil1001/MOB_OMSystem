package DBClasses;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    CommonClass cc = new CommonClass();

    private static final String DB_NAME = CommonClass.DB_Name + ".sqlite";
    //private static final String DB_NAME =  Environment.getExternalStorageDirectory()
        //    + "//.//My.db";
    private static final String TAG = "DBHelper";
    private static final int DATABASE_VERSION = 2;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.i(TAG, "Creating Table ...");
            db.beginTransaction();

            db.execSQL(DBMRegistration.CreateTable_MRegistration);
            db.execSQL(DBMCity.CreateTable_MCity);
            db.execSQL(DBMStockItems.CreateTable_MStockItems);
            db.execSQL(DBMLedger.CreateTable_MLedger);
            db.execSQL(DBTVoucherEntry.CreateTable_TVoucherEntry);
            db.execSQL(DBTStock.CreateTable_TStock);
            db.execSQL(DBTVoucherRefDetails.CreateTable_TVoucherRefDetails);

            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception ex) {
            cc.ErrorMsg("DBHelper,onCreate", ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            Log.i(TAG, "App Version Change :"+oldVersion+" To "+newVersion);
            Log.i(TAG, "OnUpgrade to Creating Table ...");

        } catch (Exception ex) {
            cc.ErrorMsg("DBHelper,onCreate", ex.getMessage());
        }
    }
}
