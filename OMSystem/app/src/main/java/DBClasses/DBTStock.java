package DBClasses;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import entity.Item;
import entity.UCSaleBills;

/**
 * Created by admin on 9/28/2017.
 */

public class DBTStock {

    CommonClass cc = new CommonClass();

    private String[] filedNames = {"PkSrNo", "PkvouchertrnNo", "FkvoucherNo", "Barcode", "ItemName", "ItemNo", "Qty", "Rate", "Mrp", "UomName", "Amount"
            , "NetRate", "NetAmount", "Taxpercentage", "TaxAmount", "DiscAmount", "HSNCode", "TempItemNo", "IGSTPer", "IGSTAmt", "CGSTPer", "CGSTAmt"
            , "SGSTPer", "SGSTAmt", "CESSPer", "CESSAmt"};

    public static String tableName = "TStock";

    public static String CreateTable_TStock = "CREATE TABLE IF NOT EXISTS "
            + tableName
            + "(PkSrNo integer primary key autoincrement "
            + ",PkvouchertrnNo INTEGER,FkvoucherNo INTEGER,Barcode VARCHAR,ItemName VARCHAR "
            + ",ItemNo INTEGER,Qty Double,Rate Double,Mrp Double,UomName VARCHAR,Amount Double,NetRate Double,NetAmount Double,Taxpercentage Double,TaxAmount Double,DiscAmount Double "
            + ",HSNCode VARCHAR,TempItemNo INTEGER,IGSTPer Double,IGSTAmt Double,CGSTPer Double,CGSTAmt Double,SGSTPer Double,SGSTAmt Double,CESSPer Double,CESSAmt Double )";

    public ContentValues AddTStock(UCSaleBills.UCTStock tsc) {
        ContentValues values = new ContentValues();

        values.put(filedNames[1], tsc.PkvouchertrnNo);
        values.put(filedNames[2], tsc.FkvoucherNo);
        values.put(filedNames[3], tsc.Barcode);
        values.put(filedNames[4], tsc.Itemname);
        values.put(filedNames[5], tsc.ItemNo);
        values.put(filedNames[6], tsc.Qty);
        values.put(filedNames[7], tsc.Rate);
        values.put(filedNames[8], tsc.Mrp);
        values.put(filedNames[9], tsc.UomName);
        values.put(filedNames[10], tsc.Amount);
        values.put(filedNames[11], tsc.NetRate);
        values.put(filedNames[12], tsc.NetAmount);
        values.put(filedNames[13], tsc.Taxpercentage);
        values.put(filedNames[14], tsc.TaxAmount);
        values.put(filedNames[15], tsc.DiscAmount);
        values.put(filedNames[16], tsc.HSNCode);
        values.put(filedNames[17], tsc.TempItemNo);
        return values;
    }


    public List<UCSaleBills.UCTStock> GetTStock(long ID) {
        String str = " Select PkSrNo, PkvouchertrnNo, FkvoucherNo, Barcode, ItemName, ItemNo, Qty, Rate, Mrp, UomName, Amount " +
                " , NetRate, NetAmount, Taxpercentage, TaxAmount, DiscAmount, HSNCode,TempItemNo " +
                " From TStock" +
                " Where FkvoucherNo=" + ID + " ";

        Cursor cursor = cc.GetDataView(str);
        if (cursor.getCount() == 0)
            return null;

        List<UCSaleBills.UCTStock> lstTstock = new ArrayList<>();
        while (cursor.moveToNext()) {
            UCSaleBills.UCTStock tvch = new UCSaleBills().new UCTStock();
            tvch.PkvouchertrnNo = cursor.getLong(1);
            tvch.FkvoucherNo = cursor.getLong(2);
            tvch.Barcode = cursor.getString(3);
            tvch.Itemname = cursor.getString(4);
            tvch.ItemNo = cursor.getLong(5);
            tvch.Qty = cursor.getDouble(6);
            tvch.Rate = cursor.getDouble(7);
            tvch.Mrp = cursor.getDouble(8);
            tvch.UomName = cursor.getString(9);
            tvch.Amount = cursor.getDouble(10);
            tvch.NetRate = cursor.getDouble(11);
            tvch.NetAmount = cursor.getDouble(12);
            tvch.Taxpercentage = cursor.getDouble(13);
            tvch.TaxAmount = cursor.getDouble(14);
            tvch.DiscAmount = cursor.getDouble(15);
            tvch.HSNCode = cursor.getString(16);
            tvch.TempItemNo = cursor.getLong(17);
            lstTstock.add(tvch);
        }

        return lstTstock;
    }

    public List<Item> GetTStock_ItesmsDetails(long ID) {
        String str = " Select MS.PkSrno,MS.ItemNo,MS.ItemName,MS.Barcode,MS.BrandName,TS.UOMName,MS.ItemNameDisp,round(MS.TaxRate,2),TS.MRP,round(TS.Rate,2), " +
                " round( TS.NetRate,2),round(TS.NetAmount,2),round(TS.Qty,2),round(TS.Amount,2),round(TS.DiscAmount,2) " +
                " From mStockItems As MS Inner Join TStock  As TS On TS.ItemNo=MS.ItemNo " +
                " Where TS.FkvoucherNo=" + ID + " ";

        Cursor cursor = cc.GetDataView(str);
        if (cursor.getCount() == 0)
            return null;

        List<Item> productList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Item item = new Item();
            item.setPkSrNo(cursor.getLong(0));
            item.setItemNo(cursor.getLong(1));
            item.setItemName(cursor.getString(2));
            item.setBarcode(cursor.getString(3));
            item.setBrandName(cursor.getString(4));
            item.setUOM(cursor.getString(5));
            item.setItemNameDisp(cursor.getString(6));
            item.setTaxRate(cursor.getDouble(7));
            item.setMrp(cursor.getDouble(8));
            item.setRate(cursor.getDouble(9));
            item.setNetRate(cursor.getDouble(10));
            item.setQty(cursor.getDouble(12));
            item.setAmount(cursor.getDouble(13));
            item.setDiscAmt(cursor.getDouble(14));
            if (item.getDiscAmt() != 0) {
                item.setDiscPer( (100*item.getDiscAmt())/(item.getRate()*item.getQty()));
            }
            productList.add(item);
        }

        return productList;
    }
}
