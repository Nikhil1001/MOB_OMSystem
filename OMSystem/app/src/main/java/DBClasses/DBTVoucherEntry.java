package DBClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import entity.UCSaleBills;

/**
 * Created by admin on 9/28/2017.
 */

public class DBTVoucherEntry {
    CommonClass cc = new CommonClass();

    private String[] filedNames = {"PkSrNo", "PkVoucherNo", "VoucherUserNo", "VoucherTypeCode", "SaleBillNo", "LedgerNo", "VoucherDate"
            , "VoucherTime", "BillAmount", "Remark", "IsCancel", "RateType", "PaymentType", "BillDisc", "ChargAmount", "StatusNo"};

    public static String tableName = "TVoucherEntry";

    public static String CreateTable_TVoucherEntry = "CREATE TABLE IF NOT EXISTS "
            + tableName
            + "(PkSrNo integer primary key autoincrement "
            + ",PkVoucherNo INTEGER,VoucherUserNo INTEGER,VoucherTypeCode INTEGER,SaleBillNo VARCHAR "
            + ",LedgerNo INTEGER,VoucherDate VARCHAR,VoucherTime VARCHAR,BillAmount Double "
            + ",Remark VARCHAR,IsCancel INTEGER,RateType INTEGER,PaymentType INTEGER,BillDisc Double,ChargAmount Double,StatusNo INTEGER)";


    public UCSaleBills.UCTVoucherEntry GetTVoucherEntry(long ID) {
        String str = " Select TVoucherEntry.PkSrNo, TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherTypeCode, TVoucherEntry.SaleBillNo, " +
                " TVoucherEntry.LedgerNo, TVoucherEntry.VoucherDate, TVoucherEntry.VoucherTime, TVoucherEntry.BillAmount, TVoucherEntry.Remark, TVoucherEntry.IsCancel, TVoucherEntry.RateType, TVoucherEntry.PaymentType," +
                " TVoucherEntry.BillDisc, TVoucherEntry.ChargAmount, TVoucherEntry.StatusNo,MLedger.LedgerName " +
                " From TVoucherEntry inner Join MLedger On TVoucherEntry.LedgerNo=MLedger.LedgerNo " +
                " Where TVoucherEntry.PkSrNo=" + ID + " ";

        Cursor cursor = cc.GetDataView(str);
        if (cursor.getCount() == 0)
            return null;
        UCSaleBills.UCTVoucherEntry tvch = null;
        while (cursor.moveToNext()) {
            tvch = new UCSaleBills().new UCTVoucherEntry();
            tvch.MobilePkSrNo = cursor.getLong(0);
            tvch.PkVoucherNo = cursor.getLong(1);
            tvch.VoucherUserNo= cursor.getLong(2);
            tvch.VoucherTypeCode= cursor.getLong(3);
            tvch.MobileBillNo= cursor.getLong(4);
            tvch.LedgerNo = cursor.getLong(5);
            tvch.VoucherDate = cc.SetCurrentDate_VoucherDate(cursor.getString(6));
            tvch.VoucherTime = cursor.getString(7);
            tvch.BilledAmount = cursor.getDouble(8);
            tvch.Reference = cursor.getString(9);
            tvch.IsCancel = (cursor.getInt(10) == 1) ? true : false;
            tvch.RateTypeNo = cursor.getLong(11);
            tvch.PayTypeNo = cursor.getLong(12);
            tvch.DiscAmt = cursor.getDouble(13);
            tvch.ChargAmount = cursor.getDouble(14);
            tvch.LedgerName = cursor.getString(16);
        }

        return tvch;
    }

    public ContentValues AddTVoucherEntry(UCSaleBills.UCTVoucherEntry tve) {
        ContentValues values = new ContentValues();
        values.put(filedNames[1], tve.PkVoucherNo);
        values.put(filedNames[2], tve.VoucherUserNo);
        values.put(filedNames[3], tve.VoucherTypeCode);
        values.put(filedNames[4], tve.VoucherUserNo);
        values.put(filedNames[5], tve.LedgerNo);
        values.put(filedNames[6], tve.VoucherDate);
        values.put(filedNames[7], tve.VoucherTime);
        values.put(filedNames[8], tve.BilledAmount);
        values.put(filedNames[9], tve.Remark);
        values.put(filedNames[10], tve.IsCancel);
        values.put(filedNames[11], tve.RateTypeNo);
        values.put(filedNames[12], tve.PayTypeNo);
        values.put(filedNames[13], tve.DiscAmt);
        values.put(filedNames[14], tve.ChargAmount);
        values.put(filedNames[15], tve.StatusNo);
        return values;
    }


    public void SaveBill_Online(UCSaleBills.UCSaleBillsList lstbill) {
        DBTVoucherEntry dbtVoucherEntry = new DBTVoucherEntry();
        DBTStock dbtStock = new DBTStock();
        DBTVoucherRefDetails dbtVoucherRefDetails = new DBTVoucherRefDetails();
        Long ID = (long) 0;
        try {


            for (UCSaleBills.UCSaleBill SB : lstbill.salesBills) {

                long PkVoucherNo = cc.ReturnLong("Select PkSrNo From TVoucherEntry Where pkvoucherno=" + SB.tvoucherentry.PkVoucherNo + "");
                if (PkVoucherNo != 0) {
                    long flagRef = cc.ReturnLong("Select Count(*) From TVoucherRefDetails  Where PkRefTrnNo<>-1 And TypeOfRef=2 And RefNo=" + PkVoucherNo+ "");
                    if (flagRef != 0)
                        continue;
                    cc.execSQL("Delete From TVoucherEntry Where PkSrNo=" + PkVoucherNo + "");
                    cc.execSQL("Delete From TStock Where FkvoucherNo=" + PkVoucherNo + "");
                    cc.execSQL("Delete From TVoucherRefDetails Where RefNo=" + PkVoucherNo + "");
                }
                ID = cc.createEntity(dbtVoucherEntry.tableName, dbtVoucherEntry.AddTVoucherEntry(SB.tvoucherentry));
                if (ID == (long) -1) {
                    throw new RuntimeException("Error In TVoucherEntry to save");
                }
                for (UCSaleBills.UCTStock ST : SB.tstock) {
                    ST.FkvoucherNo = ID;
                    long TID = cc.createEntity(dbtStock.tableName, dbtStock.AddTStock(ST));
                    if (TID == (long) -1) {
                        throw new RuntimeException("Error In TStock to save");
                    }
                }

                for (UCSaleBills.UCTVoucherRefDetails ST : SB.tVoucherrefdetails) {
                    ST.Refno = ID;
                    long TID = cc.createEntity(dbtVoucherRefDetails.tableName, dbtVoucherRefDetails.AddTVoucherRefDetails(ST));
                    if (TID == (long) -1) {
                        throw new RuntimeException("Error In tVoucherRefDetails to save");
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("SaveBill", ex.getMessage().toString());
            cc.execSQL("Delete From TStock Where FkvoucherNo=" + ID + "");
            cc.execSQL("Delete From TVoucherRefDetails Where RefNo=" + ID + "");
            cc.execSQL("Delete From TVoucherEntry Where PkSrNo=" + ID + "");


        }
    }

    public UCSaleBills.UCSaleBillsList GetBill_online() {

        DBTStock dbtStock = new DBTStock();
        DBTVoucherRefDetails dbtVoucherRefDetails = new DBTVoucherRefDetails();

        UCSaleBills.UCSaleBillsList lstBill = new UCSaleBills().new UCSaleBillsList();
        String str = " Select PkSrNo " +
                " From TVoucherEntry " +
                " Where  VoucherUserNo<>0 And StatusNo=0";

        Cursor cursor = cc.GetDataView(str);
        while (cursor.moveToNext()) {
            UCSaleBills.UCSaleBill SB = new UCSaleBills().new UCSaleBill();
            SB.tvoucherentry = GetTVoucherEntry(cursor.getLong(0));
            if(SB.tvoucherentry==null)
                continue;
            SB.tstock = dbtStock.GetTStock(cursor.getLong(0));
            SB.tVoucherrefdetails = dbtVoucherRefDetails.GetTVoucherRef(cursor.getLong(0));
            lstBill.salesBills.add(SB);
        }

        return lstBill;
    }

    public List<UCSaleBills.UCTVoucherRefDetails> GetCollection_online() {
        DBClasses.DBTVoucherRefDetails dbtVoucherRefDetails = new DBClasses.DBTVoucherRefDetails();
        String Str = " Select TR.PkSrNo From TVoucherRefDetails AS TR Left Join TVoucherEntry AS TE ON TE.PkSrNo=TR.RefNo " +
                " Where TR.PkRefTrnNo=0  And TR.TypeOFRef=2 And (TE.StatusNo=0 OR TE.StatusNo=NULL) ";
        List<UCSaleBills.UCTVoucherRefDetails> lst = new ArrayList<>();
        Cursor cursor = cc.GetDataView(Str);
        while (cursor.moveToNext()) {
            UCSaleBills.UCTVoucherRefDetails tvr = dbtVoucherRefDetails.GetCollectionData(cursor.getLong(0));
            lst.add(tvr);
        }
        return lst;
    }

    public class TReturnBill {
        public long PkSrNo;
        public long FkVoucherNo;
    }

}
