package DBClasses;


import android.content.ContentValues;

public abstract class Entity {
	protected long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * get array of filed name of entity
	 * 
	 * @return array of filed name of entity
	 */
	public abstract ContentValues values();

	/**
	 * 
	 * @return name of entity's table name
	 */
	public abstract String getTableName();

}
