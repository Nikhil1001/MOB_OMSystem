package DBClasses;

import android.content.ContentValues;

import Web.WebSevice;
import Web.WebSevice.FuctionName;
import entity.UCRegistration;

public class DBMRegistration extends Entity {

    private static String tableName = "MRegistration";
    public static String CreateTable_MRegistration = "CREATE TABLE IF NOT EXISTS "
            + tableName
            + "(PkSrNo integer primary key autoincrement "
            + ",AgentName  VARCHAR,RegNo VARCHAR,UserName VARCHAR,Password VARCHAR,Email_ID VARCHAR"
            + ",MobileNo  VARCHAR,RegDate VARCHAR,DeviceID VARCHAR,IsActive integer,IPAdress VARCHAR,StoreEmail_ID VARCHAR,StorePassword VARCHAR,StoreName VARCHAR)";
    CommonClass cc = new CommonClass();
    private String[] filedNames = {"PkSrNo", "AgentName", "RegNo", "UserName", "Password",
            "Email_ID", "MobileNo", "RegDate", "DeviceID", "IsActive", "IPAdress", "StoreEmail_ID", "StorePassword","StoreName"};

    private long mPkSrNo;
    private String mAgentName;
    private String mRegNo;
    private String mUserName;
    private String mPassword;
    private String mEmail_ID;
    private String mMobileNo;
    private String mRegDate;
    private long mIsActive;
    private String mDeviceID;
    private String mIPAdress;
    private String mStoreEmail_ID;
    private String mStorePassword;
    private String mStoreName;

    public boolean AddMRegistration(UCRegistration items) {
        boolean flag = false;
        DBMRegistration mnot = new DBMRegistration();
        mnot.setmAgentName(items.UserName);
        mnot.setmEmail_ID(items.Email_ID);
        mnot.setmUserName(items.UserID);
        mnot.setmPassword(items.Password);
        mnot.setmMobileNo(items.MobileNo);
        mnot.setmRegDate(items.RegDate);
        mnot.setmRegNo(items.RegNo);
        mnot.setmDeviceID(items.DeviceID);
        mnot.setmIsActive((items.IsActive == true) ? 1 : 0);
        mnot.setmIPAdress(items.IPAdress);
        mnot.setmStoreEmail_ID(items.StoreEmail_ID);
        mnot.setmStorePassword(items.StorePassword);
        mnot.setmStoreName(items.CompanyName);

        long ID = cc.createEntity(mnot);
        flag = true;
        return flag;
    }

    @Override
    public ContentValues values() {
        ContentValues values = new ContentValues();
        values.put(filedNames[1], mAgentName);
        values.put(filedNames[2], mRegNo);
        values.put(filedNames[3], mUserName);
        values.put(filedNames[4], mPassword);
        values.put(filedNames[5], mEmail_ID);
        values.put(filedNames[6], mMobileNo);
        values.put(filedNames[7], mRegDate);
        values.put(filedNames[8], mDeviceID);
        values.put(filedNames[9], mIsActive);
        values.put(filedNames[10], mIPAdress);
        values.put(filedNames[11], mStoreEmail_ID);
        values.put(filedNames[12], mStorePassword);
        values.put(filedNames[13], mStoreName);
        return values;
    }

    @Override
    public String getTableName() {

        return tableName;
    }

    public long getmIsActive() {
        return mIsActive;
    }

    public void setmIsActive(long mIsActive) {
        this.mIsActive = mIsActive;
    }

    public String getmDeviceID() {
        return mDeviceID;
    }

    public void setmDeviceID(String mDeviceID) {
        this.mDeviceID = mDeviceID;
    }


    public String getmAgentName() {
        return mAgentName;
    }

    public void setmAgentName(String mAgentName) {
        this.mAgentName = mAgentName;
    }

    public String getmRegNo() {
        return mRegNo;
    }

    public void setmRegNo(String mRegNo) {
        this.mRegNo = mRegNo;
    }

    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getmEmail_ID() {
        return mEmail_ID;
    }

    public void setmEmail_ID(String mEmail_ID) {
        this.mEmail_ID = mEmail_ID;
    }

    public String getmMobileNo() {
        return mMobileNo;
    }

    public void setmMobileNo(String mMobileNo) {
        this.mMobileNo = mMobileNo;
    }

    public String getmRegDate() {
        return mRegDate;
    }

    public void setmRegDate(String mRegDate) {
        this.mRegDate = mRegDate;
    }

    public long getmPkSrNo() {
        return mPkSrNo;
    }

    public void setmPkSrNo(long mPkSrNo) {
        this.mPkSrNo = mPkSrNo;
    }

    public String getIPAdress() {
        return mIPAdress;
    }

    public void setmIPAdress(String mIPAdress) {
        this.mIPAdress = mIPAdress;
    }

    public String getmStoreEmail_ID() {
        return mStoreEmail_ID;
    }

    public void setmStoreEmail_ID(String mStoreEmail_ID) {
        this.mStoreEmail_ID = mStoreEmail_ID;
    }

    public String getmStorePassword() {
        return mStorePassword;
    }

    public void setmStorePassword(String mStorePassword) {
        this.mStorePassword = mStorePassword;
    }

    public String getmStoreName() {
        return mStoreName;
    }

    public void setmStoreName(String mStoreName) {
        this.mStoreName = mStoreName;
    }


}
