package DBClasses;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import entity.DDLEntity;

public class CommonClass {

    private static final String TAG = "CommonClass";
    public static String DB_Name = "OMSystem";
    public static String DeviceID = "";
    public static long SystemReg = 0;
    public static long SystemUserType = 0;
    public static Context context;
    public static DBHelper helper;
    public static SQLiteDatabase com;
    public static String WebUrl = "";
    public static String ServerName = "";
    public static int UserType;

    public CommonClass() {

    }

    public static class UserType {
        public static int User = 0;
        public static int Admin = 1;
        public static int SuperAdmin = 2;
    }

    public static void Close() {

        helper.close();
        com.close();
    }

    public String GetDeviceId() {
        return Secure
                .getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    public boolean execSQL(String query) {
        try {
            helper = new DBHelper(context);
            com = helper.getWritableDatabase();
            com.isOpen();
            com.execSQL(query);
            return true;
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        } finally {
            com.close();
            helper.close();
        }

    }

    public long createEntity(Entity entity) {
        try {
            long id = -1;
            helper = new DBHelper(context);
            com = helper.getWritableDatabase();
            com.isOpen();
            id = com.insert(entity.getTableName(), null, entity.values());
            Log.i("createEntity",
                    "Insert New entity in " + entity.getTableName()
                            + " table [ id : " + id + "]");
            return id;
        } catch (Exception ex) {
            //	Log.i(TAG + "_createEntity", "" + ex.getMessage());
            ErrorMsg(TAG + "_createEntity", ex.getMessage());
            return -1;
        } finally {
            com.close();
            helper.close();
        }
    }

    public long createEntity(String TableName, ContentValues values) {
        try {
            long id = -1;
            helper = new DBHelper(context);
            com = helper.getWritableDatabase();
            com.isOpen();
            id = com.insert(TableName, null, values);
            Log.i("createEntity",
                    "Insert New entity in " + TableName
                            + " table [ id : " + id + "]");
            return id;
        } catch (Exception ex) {
            //	Log.i(TAG + "_createEntity", "" + ex.getMessage());
            ErrorMsg(TAG + "_createEntity", ex.getMessage());
            return -1;
        } finally {
            com.close();
            helper.close();
        }
    }

    public long UpdateEntity(String TableName, ContentValues values, String strWhere, String id) {
        try {
            long rID = -1;
            helper = new DBHelper(context);
            com = helper.getWritableDatabase();
            com.isOpen();
            rID = com.update(TableName, values, strWhere, new String[]{id});
            Log.i("update Entry",
                    "update entity in " + TableName
                            + " table [ id : " + id + "]");
            return rID;
        } catch (Exception ex) {
            Log.e(TAG, "_Update Entry" + ex.getMessage().toString());
            //ErrorMsg(TAG + "_createEntity", ex.getMessage());
            return -1;
        } finally {
            com.close();
            helper.close();
        }
    }

    public long UpdateEntity(Entity entity, String whereClause) {
        try {
            long id = -1;
            helper = new DBHelper(context);
            com = helper.getWritableDatabase();
            com.isOpen();
            id = com.update(entity.getTableName(), entity.values(),
                    whereClause, null);
            Log.i("createEntity",
                    "Insert New entity in " + entity.getTableName()
                            + " table [ id : " + id + "]");
            return id;
        } catch (Exception ex) {
            Log.i(TAG + "_createEntity", "" + ex.getMessage());
           // ErrorMsg(TAG + "_createEntity", ex.getMessage());
            return -1;
        } finally {
            com.close();
            helper.close();
        }
    }

    public Cursor GetDataView(String query) {

        try {
            helper = new DBHelper(context);
            com = helper.getWritableDatabase();
            com.isOpen();

            return com.rawQuery(query, null);
        } catch (SQLException ex) {
            Log.i(TAG + "_GetDataView", "" + ex.getMessage());
            ErrorMsg(TAG + "_GetDataView", ex.getMessage());
            return null;
        } finally {
            // com.close();
            // helper.close();
        }
    }

    public long GetLongDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTimeInMillis();
    }

    public String getStrDate(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified
        // format.
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy");

        // Create a calendar object that will convert the date and time value in
        // milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public long ReturnLong(String query) {
        Cursor cs = null;
        long ReturnValue = 0;
        try {
            helper = new DBHelper(context);
            com = helper.getWritableDatabase();
            com.isOpen();
            cs = com.rawQuery(query, null);
            if (cs != null && cs.getCount() != 0) {
                cs.moveToFirst();
                ReturnValue = cs.getLong(0);
            }

            return ReturnValue;
        } catch (SQLException ex) {
            Log.i(TAG + " ReturnLong", "" + ex.getMessage());
            ErrorMsg(TAG + "ReturnLong", ex.getMessage());
            return 0;
        } finally {

            cs.close();

        }

    }

    public String ReturnString(String query) {
        Cursor cs = null;
        String ReturnValue = "";
        try {
            helper = new DBHelper(context);
            com = helper.getWritableDatabase();
            com.isOpen();
            cs = com.rawQuery(query, null);
            if (cs != null && cs.getCount() != 0) {
                cs.moveToFirst();
                ReturnValue = cs.getString(0);
            }

            return ReturnValue;
        } catch (SQLException ex) {
            Log.i(TAG + " ReturnLong", "" + ex.getMessage());
            ErrorMsg(TAG + "ReturnLong", ex.getMessage());
            return "";
        } finally {

            cs.close();

        }

    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public java.util.Date GetDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

        java.util.Date date = null;
        try {
            date = formatter.parse(strDate);
            return date;
        } catch (Exception ex) {
            Log.i(TAG + " GetDate", "" + ex.getMessage());
            ErrorMsg(TAG + "GetDate", ex.getMessage());
            return date;
        }

    }

    public java.util.Date GetOnlyDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        java.util.Date date = null;
        try {
            date = formatter.parse(strDate);
            return date;
        } catch (Exception ex) {
            Log.i(TAG + " GetOnlyDate", "" + ex.getMessage());
            ErrorMsg(TAG + "GetOnlyDate", ex.getMessage());
            return date;
        }
    }

    public java.util.Date GetMyDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy");

        java.util.Date date = null;
        try {
            date = formatter.parse(strDate);
            return date;
        } catch (Exception ex) {
            Log.i(TAG + " GetMyDate", "" + ex.getMessage());
            ErrorMsg(TAG + "GetMyDate", ex.getMessage());
            return date;
        }

    }

    public java.util.Date ReturnDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy",
                Locale.ENGLISH);

        java.util.Date date = null;
        try {
            date = formatter.parse(strDate);
            return date;
        } catch (Exception ex) {
            Log.i(TAG + " GetMyDate", "" + ex.getMessage());
            ErrorMsg(TAG + "GetMyDate", ex.getMessage());
            return date;
        }

    }

    public String PlusDate(String strDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(strDate));
            c.add(Calendar.DATE, 1); // number of days to add
            return sdf.format(c.getTime());
        } catch (Exception ex) {
            Log.i(TAG + " PlusDate", "" + ex.getMessage());
            ErrorMsg(TAG + "PlusDate", ex.getMessage());
            return "";
        }
    }

    public String MinsDate(String strDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(strDate));
            c.add(Calendar.DATE, -1); // number of days to add
            return sdf.format(c.getTime());
        } catch (Exception ex) {
            Log.i(TAG + " MinsDate", "" + ex.getMessage());
            ErrorMsg(TAG + "MinsDate", ex.getMessage());
            return "";
        }
    }

    public int ConvertboolToInt(String BoolValue) {
        int no = 0;
        if (BoolValue.toUpperCase().equalsIgnoreCase("TRUE"))
            no = 1;
        return no;
    }

    public int ConvertboolToInt(Boolean BoolValue) {
        int no = 0;
        if (BoolValue)
            no = 1;
        return no;
    }

    public Boolean ConvertboolToInt(long BoolValue) {
        Boolean no = false;
        if (BoolValue == 1)
            no = true;
        return no;
    }

    public void SetErrorMsg(String FuctionName, String ErrorMsg) {
        String strFile = "/mnt/sdcard/./ErrorMsg/" + "ErrorMsg.txt";
        try {
            File root = new File(Environment.getExternalStorageDirectory(),
                    "ErrorMsg");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "ErrorMsg.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append("\n" + FuctionName + ":-" + ErrorMsg);
            writer.append("\n");
            writer.flush();
            writer.close();
            // Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    public boolean CreateFolder(String floderPath) {
        boolean flag = false;

        try {
            File myfile = new File(Environment.getExternalStorageDirectory()
                    .getPath(), floderPath);
            if (!myfile.exists()) {
                myfile.mkdir();
            }

            flag = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            flag = false;
        } finally {
        }

        return flag;
    }

    public boolean CsvCreating(String filename) {
        boolean flag = false;

        try {
            File myfile = new File(Environment.getExternalStorageDirectory()
                    .getPath(), filename);
            if (!myfile.exists()) {
                myfile.createNewFile();
            }

            flag = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            flag = false;
        } finally {
        }

        return flag;
    }

    public void ErrorMsg(String Msg) {

        try {
            CreateFolder(".");
            CsvCreating(".//Error.txt");

            File fout = new File(Environment.getExternalStorageDirectory()
                    .getPath(), ".//Error.txt");
            FileOutputStream fos = new FileOutputStream(fout, true);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos,
                    "UTF-8"));
            String newline = "\r\n";

            bw.write(Msg);
            bw.write(newline);
            bw.close();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
        }

    }

    public void ErrorMsg(String FunctionName, String Msg) {

        try {
            CreateFolder(".");
            CsvCreating(".//Error.txt");

            File fout = new File(Environment.getExternalStorageDirectory()
                    .getPath(), ".//Error.txt");
            FileOutputStream fos = new FileOutputStream(fout, true);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos,
                    "UTF-8"));
            String newline = "\r\n";

            bw.write(FunctionName + " :- " + Msg);
            bw.write(newline);
            bw.close();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
        }

    }


    public void GetDataBackup() {
        try {

            final String inFileName = "//data//data//" + context.getApplicationContext().getPackageName() + "//databases//"
                    + CommonClass.DB_Name + ".sqlite";
            Log.i("CommonClass", inFileName);
            File dbFile = new File(inFileName);
            FileInputStream fis = new FileInputStream(dbFile);

            String outFileName = Environment.getExternalStorageDirectory()
                    + "//.//My1.db";
            Log.i("CommonClass", outFileName);

            // Open the empty db as the output stream
            OutputStream output = new FileOutputStream(outFileName);

            // Transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }

            // Close the streams
            output.flush();
            output.close();
            fis.close();
        } catch (Exception ex) {
            Log.e("CommanClass", ex.getMessage());
            ErrorMsg(ex.getMessage());
        }
    }

    public String formatDate(int year, int month, int day) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(year, month, day);
        java.util.Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");

        return sdf.format(date);
    }

    public String GetCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        return df.format(c.getTime());
    }

    public String GetCurrentDate(String StrDateFormat) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(StrDateFormat);
        return df.format(c.getTime());
    }
    public String GetCurrentDate_VoucherDate(String StrDate) {
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
            Date date = sdf.parse(StrDate);
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format(date);
        } catch (Exception Ex) {
            return "";
        }
    }

    public String SetCurrentDate_VoucherDate(String StrDate) {
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(StrDate);
            sdf = new SimpleDateFormat("dd/MMM/yyyy");
            return sdf.format(date);
        } catch (Exception Ex) {
            return "";
        }
    }


    public ArrayList<DDLEntity> GetDropDownList(String Sql) {

        ArrayList<DDLEntity> STATUS_LIST = new ArrayList<DDLEntity>();
        Cursor ddlist = null;
        try {
            STATUS_LIST.add(new DDLEntity(0, "---select---"));
            ddlist = GetDataView(Sql);
            while (ddlist.moveToNext()) {

                STATUS_LIST.add(new DDLEntity(ddlist.getLong(0), ddlist
                        .getString(1)));
            }
        } catch (Exception ex) {
            ErrorMsg(ex.getMessage());
        } finally {
            if (ddlist != null) {
                ddlist.close();
                Close();
            }
        }
        return STATUS_LIST;
    }

    public ArrayList<DDLEntity> GetDropDownList_NoSelect(String Sql) {

        ArrayList<DDLEntity> STATUS_LIST = new ArrayList<DDLEntity>();
        Cursor ddlist = null;
        try {
           // STATUS_LIST.add(new DDLEntity(0, "---select---"));
            ddlist = GetDataView(Sql);
            while (ddlist.moveToNext()) {

                STATUS_LIST.add(new DDLEntity(ddlist.getLong(0), ddlist
                        .getString(1)));
            }
        } catch (Exception ex) {
            ErrorMsg(ex.getMessage());
        } finally {
            if (ddlist != null) {
                ddlist.close();
                Close();
            }
        }
        return STATUS_LIST;
    }

    public ArrayList<DDLEntity> GetPayType() {
        ArrayList<DDLEntity> STATUS_LIST = new ArrayList<DDLEntity>();

        STATUS_LIST.add(new DDLEntity(2, "Credit"));
        STATUS_LIST.add(new DDLEntity(1, "Cash"));
        return STATUS_LIST;
    }

    public java.util.Date SetDateAndTime(String StrDate) {
        java.util.Date today;

        DateFormat df = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault());
        java.util.Date date = null;
        try {
            date = df.parse(StrDate);
            return date;
        } catch (Exception ex) {
            Log.i(TAG + " GetDate", "" + ex.getMessage());
            //  ErrorMsg(TAG + "GetDate", ex.getMessage());
            return date;
        }
    }

    public String GetDateAndTime() {
        java.util.Date today;
        DateFormat df = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault());
        today = new java.util.Date();
        return df.format(today);
    }

    public static void MessageBox(Context cnt, String StrMess) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(cnt);
        dlgAlert.setMessage(StrMess);
        dlgAlert.setTitle("Error Message...");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }



    public static final class DBGetVal {
        public static long UserID;
        public static long PostType;
        public static long InspectorNo;

        public static String AgentName;
        public static String MobileNo;
        public static String Email_ID;
        public static String UserName;
        public static String Password;
        public static String DeviceID;
        public static String RegDate;
        public static String RegNo;
        public static long IsActive;
        public static String IpAddress;
        public static String CompanyName;


    }
}
