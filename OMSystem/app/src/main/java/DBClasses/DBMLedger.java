package DBClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import entity.Item;
import entity.UCLedger;


public class DBMLedger {

    CommonClass cc = new CommonClass();

    private String[] filedNames = {"PkSrNo", "LedgerNo", "LedgerName", "StateNo", "CityNo", "AreaNo", "MobileNo", "Address", "CityName"};

    private static String tableName = "MLedger";

    public static String CreateTable_MLedger = "CREATE TABLE IF NOT EXISTS "
            + tableName
            + "(PkSrNo integer primary key autoincrement "
            + " , LedgerNo INTEGER,LedgerName  VARCHAR "
            + " , StateNo INTEGER,CityNo INTEGER,AreaNo INTEGER,MobileNo VARCHAR,Address VARCHAR,CityName VARCHAR)";


    public void Save_MLedger(List<UCLedger> dtoItems) {
        for (int i = 0; i < dtoItems.size(); i++) {

            UCLedger items = dtoItems.get(i);
            ContentValues values = new ContentValues();
            values.put(filedNames[1], items.LedgerNo);
            values.put(filedNames[2], items.LedgerName);
            values.put(filedNames[3], items.StateNo);
            values.put(filedNames[4], items.CityNo);
            values.put(filedNames[5], items.AreaNo);
            values.put(filedNames[6], items.MobileNo);
            values.put(filedNames[7], items.Address);
            values.put(filedNames[8], items.CityName);

            long ID = cc.createEntity(tableName, values);
            if (ID == -1) {
                Log.e("", "Items Not Save :" + items.LedgerNo);
                throw new RuntimeException("Items Not Save");
            }
        }
    }

    public List<UCLedger> getBySearch(String searchText) {
        Cursor cursor = cc.GetDataView(" Select PkSrNo, LedgerNo, LedgerName||' ('||CityName||')' As LedgerName, StateNo, CityNo, AreaNo, MobileNo, Address, CityName "+
                                       " from MLedger Where LedgerName like '%" + searchText + "%'  OR CityName like '%" + searchText + "%'" +
                                       " Order By  LedgerName");
        if (cursor.getCount() == 0)
            return null;
        List<UCLedger> productList = new ArrayList<>();
        while (cursor.moveToNext()) {
            UCLedger item = new UCLedger();
            item.PkSrNo=cursor.getLong(0);
            item.LedgerNo=cursor.getLong(1);
            item.LedgerName=cursor.getString(2);
            item.StateNo=cursor.getLong(3);
            item.CityNo=cursor.getLong(4);
            item.AreaNo=cursor.getLong(5);
            item.MobileNo=cursor.getString(6);
            item.Address=cursor.getString(7);
            item.CityName=cursor.getString(8);
            productList.add(item);
        }
        return productList;
    }


}
