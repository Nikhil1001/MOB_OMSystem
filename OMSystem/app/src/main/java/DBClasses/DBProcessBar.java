package DBClasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import Web.WebSevice;


/**
 * Created by admin on 8/24/2017.
 */


public class DBProcessBar {

    public static Context cnt;

    public static String StrGetData;
    static boolean isResponse = true;

    public DBProcessBar(Context cnt) {
        this.cnt = cnt;
    }


    public String GetServiceData_Trans(String StrValue) {

        StrGetData = null;
        isResponse = true;
        AsyncTaskRunner runner = new AsyncTaskRunner();
        runner.execute(StrValue).toString();

        while (isResponse == true) {

        }
        return StrGetData;

    }



    private class AsyncTaskRunner extends AsyncTask<String, Integer, String> {

        private String resp;
        ProgressDialog progressDialog;


        @Override
        protected String doInBackground(String... params) {
//            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            return WebSevice.GetServiceData_Trans(params[0]);
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();

            StrGetData = result;
            isResponse = false;
        }


        @Override
        protected void onPreExecute() {
            isResponse = true;
            progressDialog = new ProgressDialog(cnt);
            progressDialog.setMessage("Processing Request...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }
    }

}
