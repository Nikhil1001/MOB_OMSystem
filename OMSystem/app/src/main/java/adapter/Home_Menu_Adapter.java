package adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.om.omsystem.R;

import java.util.ArrayList;

import DBClasses.CommonClass;
import entity.Item;
import entity.dto.MenuItem;


public class Home_Menu_Adapter extends ArrayAdapter<MenuItem> {
    Context context;
    int layoutResourceId;
    ArrayList<MenuItem> data = new ArrayList<MenuItem>();

    public Home_Menu_Adapter(Context context, int layoutResourceId, ArrayList<MenuItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RecordHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new RecordHolder();
            holder.ll_Home = (LinearLayout) row.findViewById(R.id.ll_home_Home);
            holder.txtTitle = (TextView) row.findViewById(R.id.lbl_home_name);
            holder.imageItem = (ImageView) row.findViewById(R.id.image_home_image);
            row.setTag(holder);
        } else {
            holder = (RecordHolder) row.getTag();
        }

        holder.ll_Home.setGravity(Gravity.CENTER);
        holder.ll_Home.setBackgroundColor(Color.parseColor("#fbdcbb"));
        holder.ll_Home.setBackgroundResource(R.drawable.home_table_row_border);
        MenuItem item = data.get(position);
        holder.txtTitle.setText(item.getMenuTitel());
        holder.imageItem.setImageBitmap(item.getMenuImage());

        return row;
    }

    static class RecordHolder {
        TextView txtTitle;
        ImageView imageItem;
        LinearLayout ll_Home;
    }
}

