package adapter;

import java.util.ArrayList;

import DBClasses.CommonClass;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.om.omsystem.R;

import entity.Item;


public class SaleBillAdapter extends BaseAdapter {

    private static final SaleBillAdapter instance = new SaleBillAdapter();
    CommonClass cc = new CommonClass();
    int pos = -1;
    private ArrayList<Item> orderListItem;
    private LayoutInflater mInflater;
    private Context context;

    public SaleBillAdapter() {
        orderListItem = new ArrayList<Item>();
    }

    public static SaleBillAdapter getInstance() {
        return instance;
    }

    public void setContext(Context context) {
        orderListItem = new ArrayList<Item>();
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return orderListItem.size();
    }

    @Override
    public Item getItem(int position) {
        return orderListItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.view_salebill_details, null);

            holder = new ViewHolder();


            holder.ItemName = (TextView) convertView.findViewById(R.id.lbl_view_salebill_ItemName);
            holder.Qty = (TextView) convertView.findViewById(R.id.lbl_view_salebill_Qty);
            holder.Rate = (TextView) convertView.findViewById(R.id.lbl_view_salebill_rate);
            holder.Amount = (TextView) convertView.findViewById(R.id.lbl_view_salebill_Amount);
            holder.SrNo = (TextView) convertView.findViewById(R.id.lbl_view_salebill_SrNo);
            holder.Disc = (TextView) convertView.findViewById(R.id.lbl_view_salebill_Disc);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Item tvch = orderListItem.get(position);
        if (tvch != null) {

            holder.ItemName.setText(tvch.getItemNameDisp());
            //   holder.MRP.setText("" + tvch.getMrp());
            holder.Qty.setText("" + tvch.getQty());
            holder.SrNo.setText("" + (position + 1));
            holder.Rate.setText("" + tvch.getRate());
            holder.Amount.setText("" + tvch.getAmount());
            holder.ItemNo = tvch.getItemNo();
            holder.PkSrno = tvch.getPkSrNo();
            holder.Disc.setText((tvch.getDiscAmt() == 0) ? "" : "" + tvch.getDiscAmt());
        } else {
            holder.ItemName.setText("");
            //     holder.MRP.setText("");
            holder.Qty.setText("");
            holder.SrNo.setText("");
            holder.Rate.setText("");
            holder.Amount.setText("");
            holder.Disc.setText("");
            holder.ItemNo = (long) 0;
            holder.PkSrno = (long) 0;
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        } else {
            convertView.setBackgroundColor(Color.rgb(211, 211, 211));
        }
        return convertView;
    }

    public void setPostion(int position) {
        pos = position;
    }

    public void removeAll() {
        orderListItem.removeAll(orderListItem);
        notifyDataSetChanged();
    }

    public void removeItems(int Pos) {
        orderListItem.remove(Pos);
        notifyDataSetChanged();
    }

    public void UpdateItems(int Pos, Item tvch) {
        orderListItem.set(Pos, tvch);
        CalculateTotal();
        notifyDataSetChanged();
    }

    public void addOrderListItem(Item tvch) {
        orderListItem.add(tvch);
        CalculateTotal();
        notifyDataSetChanged();
    }

    private void CalculateTotal() {

        for (int i = 0; i < orderListItem.size(); i++) {
            Item item = orderListItem.get(i);
            double Amount = (item.getQty() * item.getRate());
            double DiscAmt = 0, TAmount = 0, NetRate = 0, DicePer = 0;

            if (item.getDiscPer() != 0) {
                DiscAmt = ((Amount * item.getDiscPer()) / 100);
            } else {
                DiscAmt = item.getDiscAmt();
            }

            if (item.getDiscAmt() != 0) {
                DicePer = (100 * item.getDiscAmt()) / Amount;
            } else {
                DicePer = item.getDiscPer();
            }
            TAmount = Amount - DiscAmt;
            if (item.getQty() != 0)
                NetRate = TAmount / item.getQty();

            item.setNetRate(Math.round(NetRate));
            item.setDiscAmt(Math.round(DiscAmt));
            item.setDiscPer(Math.round(DicePer));
            item.setAmount(Math.round(TAmount));
            orderListItem.set(i, item);
        }
    }

    public double GetTotalAmount() {
        double Total = 0;
        for (Item item : orderListItem) {

            Total = Total + item.getAmount();
        }
        return Total;
    }

    static class ViewHolder {
        public TextView ItemName, SrNo, Qty, Rate, Amount, MRP, Disc;
        public Long ItemNo, PkSrno;
        public int pos;

    }

}