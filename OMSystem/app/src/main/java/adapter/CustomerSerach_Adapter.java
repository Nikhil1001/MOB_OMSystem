package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import com.om.omsystem.R;
import java.util.ArrayList;
import java.util.List;
import DBClasses.DBMLedger;
import entity.UCLedger;

/**
 * Created by admin on 10/2/2017.
 */

public class CustomerSerach_Adapter extends ArrayAdapter<UCLedger> {

    Context context;
    public List<UCLedger> items, tempItems, suggestions;
    DBMLedger dbmLedger;

    public CustomerSerach_Adapter(Context context, int resource, List<UCLedger> items) {
        super(context, resource, items);
        this.context = context;
        this.items = items;
        dbmLedger = new DBMLedger();
        tempItems = new ArrayList<>(items); // this makes the difference.
        suggestions = new ArrayList<UCLedger>();
    }

    @Override
    public long getItemId(int position) {
        try {
            UCLedger prod = items.get(position);
            if (prod != null) {
                return prod.LedgerNo;
            } else {
                return -1;
            }
        } catch (Exception e) {
            Log.e("AdptProduct", e.toString());
            return -1;
        }
    }

    public UCLedger getItemsDetails(int position) {
        return items.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.view_customer_search, parent, false);
        }
        UCLedger product = items.get(position);
        if (product != null) {
            TextView tvCustomerName = (TextView) view.findViewById(R.id.tvCustomerName);
            TextView tvMobileNo = (TextView) view.findViewById(R.id.tvMobileNo);
            TextView tvAddress = (TextView) view.findViewById(R.id.tvAddress);
            tvCustomerName.setText(product.LedgerName);
            tvMobileNo.setText(product.MobileNo);
            tvAddress.setText(product.Address);
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            if (resultValue == null) {
                return null;
            } else {
                String str = ((UCLedger) resultValue).LedgerName;
                return str;
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                if (suggestions != null)
                    suggestions.clear();

                suggestions = dbmLedger.getBySearch(String.valueOf(constraint));

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                List<UCLedger> filterList = (ArrayList<UCLedger>) results.values;

                items.addAll(filterList);
            }
            notifyDataSetChanged();
        }





    };
}
