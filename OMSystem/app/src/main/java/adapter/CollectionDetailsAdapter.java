package adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.om.omsystem.R;

import java.util.ArrayList;

import DBClasses.CommonClass;
import entity.DTOCollectionBill;

public class CollectionDetailsAdapter extends BaseAdapter {

    private ArrayList<DTOCollectionBill> orderListItem;

    private LayoutInflater mInflater;
    private Context context;
    CommonClass cc = new CommonClass();
    int pos = -1;

    private static final CollectionDetailsAdapter instance = new CollectionDetailsAdapter();

    public static CollectionDetailsAdapter getInstance() {
        return instance;
    }

    public CollectionDetailsAdapter() {
        orderListItem = new ArrayList<DTOCollectionBill>();
    }

    public void setContext(Context context) {
        orderListItem = new ArrayList<DTOCollectionBill>();
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return orderListItem.size();
    }

    @Override
    public DTOCollectionBill getItem(int position) {
        return orderListItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.view_collection_details, null);
            holder = new ViewHolder();
            holder.lblBillNo = (TextView) convertView.findViewById(R.id.lbl_view_collection_details_RefNo);
            holder.lblRecAmt = (TextView) convertView.findViewById(R.id.lbl_view_collection_details_BillDate);
            holder.lblBillDate = (TextView) convertView.findViewById(R.id.lbl_view_collection_details_BillAmount);
            holder.lblBillAmount = (TextView) convertView.findViewById(R.id.lbl_view_collection_details_paytype);
            holder.lblNetBal = (TextView) convertView.findViewById(R.id.lbl_view_collection_NetBal);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final DTOCollectionBill tvch = orderListItem.get(position);
        if (tvch != null) {
            holder.lblBillNo.setText(tvch.BillNo);
            holder.lblBillAmount.setText("" + tvch.BillAmount);
            holder.lblBillDate.setText(tvch.BillDate);
            holder.lblRecAmt.setText("" + tvch.TotRecAmt);
            holder.lblNetBal.setText("" + tvch.NetBal);
            holder.PkSrNo = tvch.PkSrNo;
            holder.FkVoucherNo = tvch.FkVoucherNo;
            holder.pos = position;
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        } else {
            convertView.setBackgroundColor(Color.rgb(211, 211, 211));
        }

        return convertView;
    }

    static class ViewHolder {
        public TextView lblBillNo, lblBillDate, lblBillAmount, lblRecAmt, lblNetBal;
        public Long PkSrNo, FkVoucherNo;
        public int pos;

    }

    public void setPostion(int position) {
        pos = position;
    }

    public void removeAll() {
        orderListItem.removeAll(orderListItem);
        notifyDataSetChanged();
    }

    public void addOrderListItem(DTOCollectionBill tvch) {
        orderListItem.add(tvch);
        notifyDataSetChanged();
    }


}
