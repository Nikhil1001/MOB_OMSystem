package adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.om.omsystem.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import DBClasses.CommonClass;
import entity.SearchBill;

public class SearchBillAdapter extends BaseAdapter {

    private ArrayList<SearchBill> orderListItem;

    private LayoutInflater mInflater;
    private Context context;
    CommonClass cc = new CommonClass();
    int pos = -1;

    private static final SearchBillAdapter instance = new SearchBillAdapter();

    public static SearchBillAdapter getInstance() {
        return instance;
    }

    public SearchBillAdapter() {
        orderListItem = new ArrayList<SearchBill>();
    }

    public void setContext(Context context) {
        orderListItem = new ArrayList<SearchBill>();
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return orderListItem.size();
    }

    @Override
    public SearchBill getItem(int position) {
        return orderListItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.view_search_bill, null);

            holder = new ViewHolder();


            holder.lblBillNo = (TextView) convertView.findViewById(R.id.lbl_view_search_bill_BillNo);
            holder.lblLedgerName = (TextView) convertView.findViewById(R.id.lbl_view_search_bill_LedgerName);
            holder.lblBillDate = (TextView) convertView.findViewById(R.id.lbl_view_search_bill_BillDate);
            holder.lblBillAmount= (TextView) convertView.findViewById(R.id.lbl_view_search_bill_BillAmount);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final SearchBill tvch = orderListItem.get(position);
        if (tvch != null) {
            holder.lblBillNo.setText(tvch.BillNo);
            holder.lblLedgerName.setText(tvch.LedgerName);
            holder.lblBillDate.setText(tvch.BillDate);
            holder.lblBillAmount.setText(""+tvch.BillAmount);
            holder.PkSrNo = tvch.PkSrNo;
            holder.pos = position;
        }

        if ( position % 2 == 0) {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        } else {
            convertView.setBackgroundColor(Color.rgb(211,211,211));
        }

        return convertView;
    }

    static class ViewHolder {
        public TextView lblBillNo,lblLedgerName,lblBillDate,lblBillAmount;
        public Long PkSrNo;
        public int pos;

    }

    public void setPostion(int position) {
        pos = position;
    }

    public void removeAll() {
        orderListItem.removeAll(orderListItem);
        notifyDataSetChanged();
    }

    public void addOrderListItem(SearchBill tvch) {
        orderListItem.add(tvch);
        notifyDataSetChanged();
    }


}
