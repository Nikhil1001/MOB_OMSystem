package adapter;

import android.content.Context;
import android.content.Entity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.om.omsystem.R;

import java.util.ArrayList;

import DBClasses.CommonClass;
import entity.DDLEntity;

public class DropDownAdapter1 extends BaseAdapter implements Filterable {

    private ArrayList<DDLEntity> _Contacts;
    private Context context;
    private LayoutInflater inflater;
    private ValueFilter valueFilter;
    private ArrayList<DDLEntity> mStringFilterList;

    public DropDownAdapter1(Context context, ArrayList<DDLEntity> _Contacts) {
        super();
        this.context = context;
        this._Contacts = _Contacts;
        mStringFilterList =  _Contacts;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        getFilter();
    }

    @Override
    public int getCount() {
        return _Contacts.size();
    }

    @Override
    public String getItem(int position) {
        return _Contacts.get(position).getName();
    }

    @Override
    public long getItemId(int position) {
        return _Contacts.get(position).getID();
    }

    public class ViewHolder {
        TextView tname, tplace;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.view_item_drop_down_list, null);
            holder.tname = (TextView) convertView.findViewById(R.id.lbl_dropdown);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        holder.tname.setText("" + _Contacts.get(position).getName());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if(valueFilter==null) {

            valueFilter=new ValueFilter();
        }

        return valueFilter;
    }
    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if(constraint!=null && constraint.length()>0){
                ArrayList<DDLEntity> filterList=new ArrayList<DDLEntity>();
                for(int i=0;i<mStringFilterList.size();i++){
                    if((mStringFilterList.get(i).getName().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {
                        DDLEntity contacts = new DDLEntity();
                        contacts.setName(mStringFilterList.get(i).getName());
                      //  contacts.setId(mStringFilterList.get(i).getId());
                        filterList.add(contacts);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else{
                results.count=mStringFilterList.size();
                results.values=mStringFilterList;
            }
            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            _Contacts=(ArrayList<DDLEntity>) results.values;
            notifyDataSetChanged();
        }
    }
}