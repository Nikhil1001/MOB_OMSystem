package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.om.omsystem.R;

import java.util.ArrayList;
import java.util.List;

import DBClasses.DBMStockItems;
import entity.Item;

/**
 * Created by admin on 10/2/2017.
 */

public class AdItemSearch extends ArrayAdapter<Item> {

    Context context;
    public List<Item> items, tempItems, suggestions;
    DBMStockItems dbmStockItems;

    public AdItemSearch(Context context, int resource, List<Item> items) {
        super(context, resource, items);
        this.context = context;
        this.items = items;
        dbmStockItems = new DBMStockItems();
        tempItems = new ArrayList<>(items); // this makes the difference.
        suggestions = new ArrayList<Item>();
    }

    @Override
    public long getItemId(int position) {
        try {
            Item prod = items.get(position);
            if (prod != null) {
                return prod.getItemNo();
            } else {
                return -1;
            }
        } catch (Exception e) {
            Log.e("AdptProduct", e.toString());
            return -1;
        }
    }

    public Item getItemsDetails(int position) {
        return items.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.view_item_search, parent, false);
        }
        Item product = items.get(position);
        if (product != null) {
            TextView tvProductName = (TextView) view.findViewById(R.id.tvProductName);
            TextView tvMrp = (TextView) view.findViewById(R.id.tvMrp);
            TextView tvProductUpc = (TextView) view.findViewById(R.id.tvProductUpc);
            tvProductName.setText(product.getItemNameDisp());
            tvMrp.setText(String.valueOf(product.getMrp()));
            tvProductUpc.setText(product.getBarcode());
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            if (resultValue == null) {
                return null;
            } else {
                String str = ((Item) resultValue).getItemNameDisp();
                return str;
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                if (suggestions != null)
                    suggestions.clear();

                suggestions = dbmStockItems.getItemListBySearch(String.valueOf(constraint));

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                List<Item> filterList = (ArrayList<Item>) results.values;

                items.addAll(filterList);
            }
            notifyDataSetChanged();
        }
    };
}
