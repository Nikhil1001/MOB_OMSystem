package adapter;

import java.util.ArrayList;

import entity.DDLEntity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DropDownAdapter extends ArrayAdapter<DDLEntity> {

    private Context context;
    private ArrayList<DDLEntity> statuses;
    public Resources res;

    DDLEntity currRowVal = null;
    LayoutInflater inflater;

    public DropDownAdapter(Context context, int textViewResourceId,
                           ArrayList<DDLEntity> statuses, Resources resLocal) {
        super(context, textViewResourceId, statuses);
        this.context = context;
        this.statuses = statuses;
        this.res = resLocal;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public DDLEntity getItem(int position) {
        return statuses.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(com.om.omsystem.R.layout.dropdownitems,
                parent, false);
        currRowVal = null;
        currRowVal = (DDLEntity) statuses.get(position);
        TextView label = (TextView) row
                .findViewById(com.om.omsystem.R.id.spinnerItem);
        label.setText(currRowVal.getName());
        return row;
    }

    public void removeAll() {
        statuses.removeAll(statuses);
        notifyDataSetChanged();
    }

    public int GetIndex(long ID,DropDownAdapter adpter) {
        int Value=0;
        for (int position = 0; position < adpter.getCount(); position++) {
            if (adpter.getItem(position).getID() == ID) {
                Value = position;
                break;
            }
        }
        return Value;
    }
}