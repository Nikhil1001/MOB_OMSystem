package adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.om.omsystem.R;

import java.util.ArrayList;

import DBClasses.CommonClass;
import entity.DTOCollectionBill;
import entity.SearchBill;
import entity.UCSaleBills;

public class CollectionBillAdapter extends BaseAdapter {

    private ArrayList<DTOCollectionBill> orderListItem;

    private LayoutInflater mInflater;
    private Context context;
    CommonClass cc = new CommonClass();
    int pos = -1;

    private static final CollectionBillAdapter instance = new CollectionBillAdapter();

    public static CollectionBillAdapter getInstance() {
        return instance;
    }

    public CollectionBillAdapter() {
        orderListItem = new ArrayList<DTOCollectionBill>();
    }

    public void setContext(Context context) {
        orderListItem = new ArrayList<DTOCollectionBill>();
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return orderListItem.size();
    }

    @Override
    public DTOCollectionBill getItem(int position) {
        return orderListItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.view_collection, null);

            holder = new ViewHolder();


            holder.lblBillNo = (TextView) convertView.findViewById(R.id.lbl_view_collection_BillNo);
            holder.lblRecAmt = (TextView) convertView.findViewById(R.id.lbl_view_collection_RecAmt);
            holder.lblBillDate = (TextView) convertView.findViewById(R.id.lbl_view_collection_BillDate);
            holder.lblBillAmount= (TextView) convertView.findViewById(R.id.lbl_view_collection_BillAmount);
            holder.lblNetBal= (TextView) convertView.findViewById(R.id.lbl_view_collection_NetBal);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final DTOCollectionBill tvch = orderListItem.get(position);
        if (tvch != null) {
            holder.lblBillNo.setText(tvch.BillNo);
            holder.lblBillAmount.setText(""+tvch.BillAmount);
            holder.lblBillDate.setText(tvch.BillDate);
            holder.lblRecAmt.setText(""+tvch.TotRecAmt);
            holder.lblNetBal.setText(""+tvch.NetBal);
            holder.PkSrNo = tvch.PkSrNo;
            holder.FkVoucherNo=tvch.FkVoucherNo;
            holder.pos = position;
        }

        if ( position % 2 == 0) {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        } else {
            convertView.setBackgroundColor(Color.rgb(211,211,211));
        }

        return convertView;
    }

    static class ViewHolder {
        public TextView lblBillNo,lblBillDate,lblBillAmount,lblRecAmt,lblNetBal;
        public Long PkSrNo,FkVoucherNo;
        public int pos;

    }

    public void setPostion(int position) {
        pos = position;
    }

    public void removeAll() {
        orderListItem.removeAll(orderListItem);
        notifyDataSetChanged();
    }

    public void addOrderListItem(DTOCollectionBill tvch) {
        orderListItem.add(tvch);
        notifyDataSetChanged();
    }

    public ArrayList<DTOCollectionBill> GetOrderList()
    {
        return orderListItem;
    }

    public double getNetBal()
    {
        double Amt=0;
        for (DTOCollectionBill items:orderListItem)
        {
         Amt=Amt+items.NetBal;
        }
        return Amt;
    }

}
