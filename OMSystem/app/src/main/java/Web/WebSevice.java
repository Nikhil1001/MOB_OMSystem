package Web;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.om.omsystem.HomeActivity;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import DBClasses.CommonClass;
import DBClasses.CommonClass.DBGetVal;


public class WebSevice {
    private static final String TAG = "WebSevice";

    // private static String SOAP_ACTION =
    // "http://m.sgkdevelopers.com/ExecuteProcedure";
    // private static String NAMESPACE
    // ="http://m.sgkdevelopers.com/ServerAccess.asmx";
    // private static String METHOD_NAME = "ExecuteProcedure";
    //public static String WebUrl = "http://scm-mahaexcise.in/complaint/WS/MasterService.asmx?WSDL";
    private static final WebSevice instance = new WebSevice();
    public static String WebUrl = "http://" + DBGetVal.IpAddress + "/api";
    public static String WebUrl_Trans = "http://scm-mahaexcise.in//dete/WS//TransService.asmx";
    // public static String WebUrl =
    // "http://192.168.1.116/SgkDev/ServerAccess.asmx?WSDL";
    private static String SOAP_ACTION = "http://tempuri.org/ExecuteProcedure";
    private static String NAMESPACE = "http://tempuri.org/";
    private static String METHOD_NAME = "ExecuteProcedure";

    private WebSevice() {

    }

    public static WebSevice getInstance() {
        return instance;
    }

    private static String ExecuteProcrdure(String value)
            throws XmlPullParserException, IOException {
        CommonClass cc = new CommonClass();
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("val", value);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;
        HttpTransportSE androidHttpTransport = new HttpTransportSE(WebUrl);
        try {
            androidHttpTransport.call(SOAP_ACTION, envelope);
        } catch (Exception ex) {
            cc.ErrorMsg("WebService,ExecuteProcrdure", ex.getMessage());
        }
        SoapObject result = (SoapObject) envelope.bodyIn;
        if (result != null) {

            return result.getProperty(0).toString();

        } else {
            Log.i(TAG, "Bad commnucation");
        }
        return "";
    }

    private static String ExecuteProcrdure(String StrWebUrl, String value)
            throws XmlPullParserException, IOException {
        CommonClass cc = new CommonClass();
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("val", value);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;
        HttpTransportSE androidHttpTransport = new HttpTransportSE(StrWebUrl);
        try {
            androidHttpTransport.call(SOAP_ACTION, envelope);
        } catch (Exception ex) {
            Log.e("WebService", ex.getMessage());

        }
        SoapObject result = (SoapObject) envelope.bodyIn;
        if (result != null) {
            return result.getProperty(0).toString();
        } else {
            Log.i(TAG, "Bad commnucation:SoapObject is Null response");
        }
        return "";
    }

    private static String ExecuteProcrdure_Json(ArrayList<Attribute> attributeLsit, String MethodName)
            throws XmlPullParserException, IOException {
        SOAP_ACTION = "http://tempuri.org/" + MethodName;
        CommonClass cc = new CommonClass();
        SoapObject request = new SoapObject(NAMESPACE, MethodName);
        for (int i = 0; i < attributeLsit.size(); i++) {
            try {
                Attribute dd = attributeLsit.get(i);
                request.addProperty(dd.getKey(), dd.getValue());
            } catch (Exception e) {
                e.printStackTrace();
            }
            //request.addProperty()
        }
        //request.addProperty("val", value);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;
        HttpTransportSE androidHttpTransport = new HttpTransportSE(WebUrl);
        try {
            androidHttpTransport.call(SOAP_ACTION, envelope);
        } catch (Exception ex) {
            cc.ErrorMsg("WebService,ExecuteProcrdure", ex.getMessage());
        }
        SoapObject result = (SoapObject) envelope.bodyIn;
        if (result != null) {

            return result.getProperty(0).toString();

        } else {
            Log.i(TAG, "Bad commnucation");
        }
        return "";
    }

    public static String GetServiceData(String StrFuction) {

        String StrReturn = "";
        CommonClass cc = new CommonClass();
        String stocks = null;
        try {
            stocks = ExecuteProcrdure(StrFuction);
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            cc.ErrorMsg("MySerice,GetServiceData: ", e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            cc.ErrorMsg("MySerice,GetServiceData: ", e.getMessage());
            e.printStackTrace();
        }
        if (stocks != null && !stocks.equalsIgnoreCase("")) {
            if (!stocks.equalsIgnoreCase("-1")) {
                if (!stocks.toUpperCase().equalsIgnoreCase("FAIL")) {
                    StrReturn = stocks;
                    cc.ErrorMsg("GetServiceData" + StrFuction);
                    cc.ErrorMsg("GetServiceData" + stocks);
                }
            }
        } else {
            StrReturn = "";
        }
        return StrReturn;
    }

    public static String GetServiceData_Trans(String StrValue) {

        Log.i("WebService", StrValue);
        String StrReturn = "";
        CommonClass cc = new CommonClass();
        String stocks = null;
        try {
            stocks = ExecuteProcrdure(WebUrl_Trans, StrValue);
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            Log.e("GetServiceData", e.getMessage());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("GetServiceData", e.getMessage());
            e.printStackTrace();
        }
        if (stocks != null && !stocks.equalsIgnoreCase("")) {
            if (!stocks.equalsIgnoreCase("-1")) {
                if (!stocks.toUpperCase().equalsIgnoreCase("FAIL")) {
                    StrReturn = stocks;
                    Log.e("GetServiceData", stocks);
                }
            }
        } else {
            StrReturn = "";
        }
        return StrReturn;
    }

    public static void ShowMessage(String StrValue) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(CommonClass.context.getApplicationContext());
        dlgAlert.setMessage(StrValue);
        dlgAlert.setTitle("OM System");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    public static String GetService_Json_Data(ArrayList<Attribute> attributeLsit, String METHOD) {

        String StrReturn = "";
        CommonClass cc = new CommonClass();
        String stocks = null;
        try {
            stocks = ExecuteProcrdure_Json(attributeLsit, METHOD);
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            cc.ErrorMsg("MySerice,GetServiceData: ", e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            cc.ErrorMsg("MySerice,GetServiceData: ", e.getMessage());
            e.printStackTrace();
        }
        if (stocks != null && !stocks.equalsIgnoreCase("")) {
            if (!stocks.equalsIgnoreCase("-1")) {
                if (!stocks.toUpperCase().equalsIgnoreCase("FAIL")) {
                    StrReturn = stocks;
                    //	cc.ErrorMsg("GetServiceData" + StrFuction);
                    cc.ErrorMsg("GetServiceData" + stocks);
                }
            }
        } else {
            StrReturn = "";
        }
        return StrReturn;
    }

    public static String GetRequest(String stringUrl) {
        String result;
        try {

            String inputLine;
            //Create a URL object holding our url
            URL myUrl = new URL(WebUrl + "" + stringUrl);
            //Create a connection
            HttpURLConnection connection = (HttpURLConnection)
                    myUrl.openConnection();
            //Set methods and timeouts
            connection.setRequestMethod("GET");
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Pos_Type", "MPOS");
            if (DBGetVal.RegNo != null)
                connection.setRequestProperty("RegNo", DBGetVal.RegNo);
            connection.connect();
            int statusCode = connection.getResponseCode();
            if (statusCode == 200) {
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();
                while ((inputLine = reader.readLine()) != null) {
                    stringBuilder.append(inputLine);
                }
                reader.close();
                streamReader.close();
                result = stringBuilder.toString();
            } else {
                InputStream inputStream1 = new BufferedInputStream(connection.getErrorStream());
                String Str1 = convertStreamToString(inputStream1);
                JSONObject obj1 = new JSONObject(Str1);
                throw new RuntimeException(obj1.getString("Message"));
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return result;

    }

    public static String PostRequest(String Url, String Body) {

        String response = "";
        HttpURLConnection urlConnection = null;
        URL url = null;
        try {
            // This is getting the url from the string we passed in

            url = new URL(WebUrl + "" + Url);

            // Create the urlConnection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Pos_Type", "MPOS");
            if (DBGetVal.RegNo != null)
                urlConnection.setRequestProperty("RegNo", DBGetVal.RegNo);
            urlConnection.connect();

            // Send the post body
            if (Body != null) {
                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(Body);
                writer.flush();
            }

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                response = convertStreamToString(inputStream);
                Log.i("Web Service Response", response);
            } else {
                InputStream inputStream1 = new BufferedInputStream(urlConnection.getErrorStream());
                String Str = convertStreamToString(inputStream1);
                JSONObject obj = new JSONObject(Str);
                throw new RuntimeException(obj.getString("Message"));
            }

        } catch (Exception e) {
            Log.d(TAG, e.getLocalizedMessage());
            throw new RuntimeException(e.getMessage());
        } finally {

            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return response;
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static class FuctionName {
        public static int ComplaintType = 1;
        public static int District = 2;
        public static int Taluka = 3;
        public static int PoliceStation = 4;
        public static int InspectorList = 5;
        public static int VehicleType = 6;
        public static int State = 7;
        public static int StatusType = 8;
        public static int SeizedType = 9;
        public static int City = 10;
    }

    public class Attribute {
        private String Key, Value;

        public String getValue() {
            return Value;
        }

        public void setValue(String value) {
            Value = value;
        }

        public String getKey() {

            return Key;
        }

        public void setKey(String key) {
            Key = key;
        }
    }


}
