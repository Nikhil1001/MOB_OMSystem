package com.om.omsystem;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import DBClasses.CommonClass;
import DBClasses.MyException;
import fragmentclasses.frag_collection;
import fragmentclasses.frag_home;
import fragmentclasses.frag_login;
import fragmentclasses.frag_registration;
import fragmentclasses.frag_salebill;

import static DBClasses.CommonClass.context;
import DBClasses.CommonClass.DBGetVal;


public class HomeActivity extends AppCompatActivity {

    CommonClass cc = new CommonClass();


    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    // nav drawer title
    private CharSequence mDrawerTitle;
    public static int pos = 0;
    // used to store app title
    private CharSequence mTitle;
    private ProgressDialog progress;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    public final static int Fragment_Welcome = 0;
    public final static int Fragment_Login = 1;
    public final static int Fragment_Menu = 2;
    public final static int Fragment_home = 3;
    public final static int Fragment_Reg = 4;
    public final static int Fragment_SaleBill = 5;
    public final static int Fragment_Collection = 6;
    public static int FragmentPos_Current = 0;
    public static long CrimeNo = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Bundle bundle = getIntent().getExtras();


        if (savedInstanceState == null) {

            try {
                if (bundle != null) {
                    String venName = bundle.getString("BackSarchBill");
                    if (venName.equalsIgnoreCase("SearchBill") == true) {
                        displayView(Fragment_Menu, 0);
                        return;
                    }
                }
                UpdateSetting();
                displayView(Fragment_Login, 0);

            } catch (Exception ex) {
                ShowMessage(ex.getMessage());
                displayView(Fragment_Reg, 0);
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
     /*   switch (item.getItemId()) {

            case R.id.action_Refresh:
                displayView(pos, 0);
                return true;
            case R.id.action_UpdateMaster:
                //UpdateMaster();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }*/
        return true;
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls

    }

    @Override
    public void onBackPressed() {

        //DispalyDialog1("Are you sure you want to exit.?");
        CallBack();
    }

    private void CallBack() {

        switch (FragmentPos_Current) {
            case Fragment_Welcome:
                DispalyDialog1("Are you sure you want to exit.?");
                break;
            case Fragment_Login:
                getActionBar().hide();
                displayView(Fragment_Welcome, 0);
                break;
            case Fragment_Menu:
                displayView(Fragment_Welcome, 0);
                break;
            default:
                DispalyDialog1("Are you sure you want to exit.?");
        }
    }

    public void DispalyDialog1(String StrMess) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(StrMess);

        alertDialogBuilder.setPositiveButton("No",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        arg0.dismiss();
                    }
                });

        alertDialogBuilder.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        // android.os.Process.killProcess(android.os.Process
                        // .myPid());

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private Fragment getFragment(int position) {
        Fragment fragment = null;
        getSupportActionBar().show();
        switch (position) {
            case Fragment_Reg:
                fragment = new frag_registration();
                break;
            case Fragment_Login:
                fragment = new frag_login();
                break;
            case Fragment_Menu:
                fragment = new frag_home();
                getSupportActionBar().setTitle(DBGetVal.CompanyName);
              //  this.setTitle(DBGetVal.CompanyName);
                break;
            case Fragment_Collection:
                fragment = new frag_collection();
                getSupportActionBar().hide();
                break;
            case Fragment_SaleBill:
                fragment = new frag_salebill();
                getSupportActionBar().hide();
                break;

            default:
                break;
        }
        return fragment;
    }

    public void displayView(int position_new, int position_current) {
        Fragment fragment = null;

        FragmentPos_Current = position_new;
        fragment = getFragment(position_new);
        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

        } else {

            // Log.e(TAG, "Error in creating fragment");
        }
    }

    public void displayView(int position_new, int position_current,
                            Bundle bundle) {
        Fragment fragment = null;
        FragmentPos_Current = position_new;
        fragment = getFragment(position_new);
        if (fragment != null) {
            if (bundle != null)
                fragment.setArguments(bundle);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

        } else {
            Log.e("HomeActivity", "Error in creating fragment");
        }

    }


    public void download() {
        progress = new ProgressDialog(this);
        progress.setMessage("Downloading Music");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setProgress(0);
        progress.show();

        final int totalProgressTime = 100;
        final Thread t = new Thread() {
            @Override
            public void run() {
                int jumpTime = 0;

                while (jumpTime < totalProgressTime) {
                    try {
                        sleep(200);
                        jumpTime += 5;
                        progress.setProgress(jumpTime);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();
    }

    private void turnGpsOn() {

        String beforeEnable = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        String newSet = String.format("%s,%s",
                beforeEnable,
                LocationManager.GPS_PROVIDER);
        try {
            Settings.Secure.putString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED,
                    newSet);
        } catch (Exception e) {
        }
    }

    public void UpdateSetting() {
        try {
            Cursor cs = cc.GetDataView("Select AgentName,RegNo, UserName,Password,Email_ID,MobileNo,RegDate,IsActive,IPAdress,StoreName "+
                    " From MRegistration ");
            if (cs.getCount() == 0)
                throw new RuntimeException("This Devices not Register,Please register the Devices");
            cs.moveToNext();
            DBGetVal.AgentName = cs.getString(0);
            DBGetVal.RegNo = cs.getString(1);
            DBGetVal.UserName = cs.getString(2);
            DBGetVal.Password = cs.getString(3);
            DBGetVal.Email_ID = cs.getString(4);
            DBGetVal.MobileNo = cs.getString(5);
            DBGetVal.RegDate = cs.getString(6);
            DBGetVal.IsActive = cs.getLong(7);
            DBGetVal.IpAddress=cs.getString(8);
            DBGetVal.CompanyName=cs.getString(9);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }

    }

    public void ShowMessage(String StrValue) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        dlgAlert.setMessage(StrValue);
        dlgAlert.setTitle("Crime Registration");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();

    }



}

