package com.om.omsystem;

import android.content.Entity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import DBClasses.CommonClass;
import Web.WebSevice;
import entity.UCRegistration;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressBar;
    private int progressStatus = 0;
    private Handler handler = new Handler();
    CommonClass cc=new CommonClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CommonClass.context=this;

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

      /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    handler.post(new Runnable() {
                        public void run() {

                        }
                    });
                    try {

                        Thread.sleep(10);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                progressStatus = 0;
                CallIntent();
            }

        }).start();
    }

    public void CallIntent() {
        boolean b = false;
        //b = this.deleteDatabase(CommonClass.DB_Name + ".sqlite");

//        ArrayList<Attribute> ddls = new ArrayList<Attribute>();
////        Attribute dd = new Attribute();
////        dd.setKey("city");
////        dd.setValue("1");
////        ddls.add(dd);
//
//        String StrDate =WebSevice.GetService_Json_Data(ddls, "GetCities");
//        try {
//
//            VarCity[] cities = DBJson.fromJson(StrDate,VarCity[].class);
//
//           Log.d("My App", ""+cities.length);
//
//        } catch (Throwable t) {
//            Log.e("My App", "Could not parse malformed JSON: \"" + StrDate + "\"");
//        }

        //UpdateMaster();
        cc.GetDataBackup();
        Intent downloadIntent = new Intent(this, HomeActivity.class);
        startActivity(downloadIntent);
        this.finish();
    }
}
