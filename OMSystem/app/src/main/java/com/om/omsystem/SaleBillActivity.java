package com.om.omsystem;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import fragmentclasses.frag_salebill;
import fragmentclasses.frag_search_bill;

public class SaleBillActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_bill);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.getString("Bill_Menu").equalsIgnoreCase("0")) {
                displayView(0, -1, null);
            } else {
                displayView(1, -1, null);
            }
        } else {
            displayView(1, -1, null);
        }
    }

    private void loadFragment() {
        Fragment fragment = getFragment(0);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame, fragment).commit();

        invalidateOptionsMenu();
    }

    private Fragment getFragment(int position_new) {

        switch (position_new) {
            case 0:
                Fragment fragment = new frag_salebill();
                return fragment;
            case 1:
                Fragment fragment1 = new frag_search_bill();
                return fragment1;
            default:
                return null;
        }
    }

    public void displayView(int position_new, int position_current,
                            Bundle bundle) {
        Fragment fragment = null;
        //FragmentPos_Current = position_new;
        fragment = getFragment(position_new);
        if (fragment != null) {
            if (bundle != null)
                fragment.setArguments(bundle);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame, fragment).commit();

        } else {
            Log.e("HomeActivity", "Error in creating fragment");
        }

    }

    @Override
    public void onBackPressed() {

        DispalyDialog1("Are you sure you want to exit.?");
    }

    public void DispalyDialog1(String StrMess) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(StrMess);

        alertDialogBuilder.setPositiveButton("No",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        arg0.dismiss();
                    }
                });

        alertDialogBuilder.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        // android.os.Process.killProcess(android.os.Process
                        // .myPid());

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
