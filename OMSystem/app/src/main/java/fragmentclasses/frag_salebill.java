package fragmentclasses;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.om.omsystem.HomeActivity;
import com.om.omsystem.R;
import com.om.omsystem.SaleBillActivity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import DBClasses.CommonClass;
import DBClasses.DBMRegistration;
import DBClasses.DBMStockItems;
import DBClasses.DBTStock;
import DBClasses.DBTVoucherEntry;
import DBClasses.DBTVoucherRefDetails;
import Web.WebSevice;
import adapter.AdItemSearch;
import adapter.CustomerSerach_Adapter;
import adapter.DropDownAdapter;
import adapter.SaleBillAdapter;
import entity.DDLEntity;
import entity.Item;
import entity.UCLedger;
import entity.UCRegistration;
import entity.UCSaleBills;

import static android.app.Activity.RESULT_OK;

public class frag_salebill extends Fragment implements View.OnClickListener {

    public static Button str_GetDate;
    AutoCompleteTextView txtItemsSarch;
    EditText txtQty;
    Button btnItemAdd;
    Boolean IsBarCodeScan = false;
    Context context;
    CommonClass cc = new CommonClass();
    TextView lblItemCount, lblTotalAmount;
    ListView lstSaleBill;
    String Tag = "frag_SaleBill :";
    CustomDialogClass customDialogClass;
    Custom_SaveBill custom_SaveBill;
    SaleBillAdapter saleBillAdapter;
    //DropDownAdapter ad_Customer;
    DropDownAdapter ad_PayType;
    ImageView img_saveBill, img_Back;
    Calendar calendar = Calendar.getInstance();
    int yy = calendar.get(Calendar.YEAR);
    int mm = calendar.get(Calendar.MONTH);
    int dd = calendar.get(Calendar.DAY_OF_MONTH);
    DBTVoucherEntry dbtVoucherEntry = new DBTVoucherEntry();
    DBTStock dbtStock = new DBTStock();
    UCSaleBills.UCTVoucherEntry tvch = new UCSaleBills().new UCTVoucherEntry();


    long PkVoucherNo = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_salebill,
                container, false);

        setRetainInstance(true);
        context = getActivity();

        //region Initalize Control
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.btn_salebill_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                customDialogClass = new CustomDialogClass(getActivity());
                customDialogClass.show();
            }
        });

        img_Back = (ImageView) rootView.findViewById(R.id.img_sale_bill_Back);
        img_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // ((SaleBillActivity) getActivity()).displayView(1, 0, null);

                Intent downloadIntent = new Intent(getActivity(), HomeActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("BackSarchBill", "SearchBill");
                downloadIntent.putExtras(bundle);
                startActivity(downloadIntent);
                getActivity().finish();
            }
        });

        lstSaleBill = (ListView) rootView.findViewById(R.id.lv_bill_product);
        saleBillAdapter = new SaleBillAdapter();
        saleBillAdapter.setContext(getActivity());
        lstSaleBill.setAdapter(saleBillAdapter);
        lstSaleBill.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                               @Override
                                               public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                   customDialogClass = new CustomDialogClass(getActivity(), i);
                                                   customDialogClass.show();

                                               }
                                           }
        );

        lstSaleBill.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()

                                               {
                                                   @Override
                                                   public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i,
                                                                                  long l) {
                                                       AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                       builder.setMessage("Do you want to delete this items?")
                                                               .setCancelable(false)
                                                               .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                                   public void onClick(DialogInterface dialog, int id) {
                                                                       // finish();
                                                                       saleBillAdapter.removeItems(i);
                                                                       saleBillAdapter.notifyDataSetChanged();
                                                                       lblItemCount.setText("" + saleBillAdapter.getCount());
                                                                       lblTotalAmount.setText("" + saleBillAdapter.GetTotalAmount());
                                                                   }
                                                               })
                                                               .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                                   public void onClick(DialogInterface dialog, int id) {
                                                                       //  Action for 'NO' Button
                                                                       dialog.cancel();
                                                                   }
                                                               });
                                                       builder.show();
                                                       return true;
                                                   }
                                               }

        );


        lblItemCount = (TextView) rootView.findViewById(R.id.tv_ProductCount);
        lblTotalAmount = (TextView) rootView.findViewById(R.id.tv_GrandTotal);

//endregion

        //region FillCustomer And PayType


        //  ArrayList<DDLEntity> STATUS_LIST = cc
        //        .GetDropDownList("Select LedgerNo,LedgerName From MLedger order by LedgerName");
        // ad_Customer = new DropDownAdapter(getActivity(),
        //       android.R.layout.simple_expandable_list_item_1, STATUS_LIST,
        //     getResources());

        ArrayList<DDLEntity> Lis_PayType = cc.GetPayType();
        ad_PayType = new DropDownAdapter(getActivity(),
                android.R.layout.simple_expandable_list_item_1, Lis_PayType,
                getResources());


        //endregion


        img_saveBill = (ImageView) rootView.findViewById(R.id.iv_CheckOut);
        img_saveBill.setOnClickListener(this);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            PkVoucherNo = bundle.getLong("PkSrNo", 0);
            if (PkVoucherNo != 0) {
                FillControl(PkVoucherNo);
            }
        }
        return rootView;
    }

    private void FillControl(long ID) {

        tvch = dbtVoucherEntry.GetTVoucherEntry(ID);
        saleBillAdapter.removeAll();
        List<Item> list_items = dbtStock.GetTStock_ItesmsDetails(ID);
        for (Item items : list_items) {
            saleBillAdapter.addOrderListItem(items);
        }
        saleBillAdapter.notifyDataSetChanged();
        lblItemCount.setText("" + saleBillAdapter.getCount());
        lblTotalAmount.setText("" + saleBillAdapter.GetTotalAmount());
    }

    @Override
    public void onClick(View v) {
        // default method for handling onClick Events..
        switch (v.getId()) {
            case R.id.iv_CheckOut:
                if (saleBillAdapter.GetTotalAmount() != 0) {
                    custom_SaveBill = new Custom_SaveBill(getActivity());
                    custom_SaveBill.show();
                } else {
                    ShowMessage("Atleast one item required.");
                }
                break;

        }
    }

    //endregion


    //region ItemSearch Related Methods

    public void ShowMessage(String StrValue) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
        dlgAlert.setMessage(StrValue);
        dlgAlert.setTitle("Sale Bill");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    //region Date Dialog
    public void SetDate(Button button) {
        str_GetDate = button;
        DialogFragment newFragment;
        newFragment = new SelectDateFragmentFrmdate();
        newFragment.show(getActivity().getFragmentManager(),
                "Datepicker");
    }


    //endregion

    public void populateSetDate(int year, int month, int day,
                                Boolean IsFromDate, DatePicker dtview) {
        yy = year;

        dd = day;
        try {
            if (IsFromDate) {
                mm = month;
                str_GetDate.setText(cc.formatDate(year, month, day));
                //BtnDate.setText(cc.formatDate(year, month, day));
            }

        } catch (Exception e) {
            Log.e("Report", "Error : " + e.getMessage());
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                txtItemsSarch.requestFocus();
                txtItemsSarch.setText(contents.toString());
                txtQty.setText("1");
                IsBarCodeScan = true;

                txtQty.requestFocus();
                btnItemAdd.requestFocus();

            }
        }
    }

    //Item Serach Menthod
    public class CustomDialogClass extends Dialog implements
            android.view.View.OnClickListener {

        static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
        public Activity c;
        public Dialog d;
        public Button btnCancel;
        CustomDialogClass customDialogClass;
        List<Item> mProductList = new ArrayList<>();
        AdItemSearch itemSearch;
        EditText txtRate, txtDisc;
        String imagePath;
        Item item;

        boolean IsItemDisplay = false;
        ImageButton imgbtnSercharBarcode;
        int Pos = 0;

        public CustomDialogClass(Activity a) {
            super(a);
            this.c = a;
            itemSearch = new AdItemSearch(a, R.layout.activity_main, mProductList);
        }

        public CustomDialogClass(Activity a, String imagePath) {
            super(a);
            this.c = a;
            this.imagePath = imagePath;
        }

        public CustomDialogClass(Activity a, int Pos) {
            super(a);
            this.c = a;
            itemSearch = new AdItemSearch(a, R.layout.activity_main, mProductList);
            this.item = item;
            IsItemDisplay = true;
            this.Pos = Pos;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            customDialogClass = this;

            this.setCancelable(false);
            this.setTitle("Serach Items");

            setContentView(R.layout.dialog_itemserach);
            txtItemsSarch = (AutoCompleteTextView) findViewById(R.id.ac_ProductSearch);
            txtItemsSarch.setAdapter(itemSearch);
            txtItemsSarch.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
                    SearchItems(itemSearch.getItemsDetails(pos));
                }

            });

            txtItemsSarch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        // code to execute when EditText loses focus
                        if (IsBarCodeScan == false)
                            return;

                        if (txtItemsSarch.getText().toString().equalsIgnoreCase(""))
                            return;

                        try {
                            DBMStockItems dbmStockItems = new DBMStockItems();
                            SearchItems(dbmStockItems.BarcodeSerach(txtItemsSarch.getText().toString()));

                        } catch (Exception ex) {
                            Toast.makeText(getActivity(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
                            txtItemsSarch.setText("");
                            txtQty.setText("");
                            txtRate.setText("");
                        }
                        IsBarCodeScan=false;
                    }
                }
            });

            btnItemAdd = (Button) findViewById(R.id.btn_ItemSearch_Add);
            btnItemAdd.setOnClickListener(this);
            Button btnItemCancel = (Button) findViewById(R.id.btn_ItemSearch_Cancel);
            btnItemCancel.setOnClickListener(this);

            txtQty = (EditText) findViewById(R.id.txt_ItemSearch_Qty);
            txtRate = (EditText) findViewById(R.id.txt_ItemSearch_Rate);
            txtDisc = (EditText) findViewById(R.id.txt_ItemSearch_Disc);

            txtDisc.setText("0");

            txtItemsSarch.requestFocus();
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            if (IsItemDisplay == true)
                SearchItems(saleBillAdapter.getItem(Pos));

            imgbtnSercharBarcode = (ImageButton) findViewById(R.id.img_SearchBarcode);
            imgbtnSercharBarcode.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_ItemSearch_Add:
                    try {
                        if (Validation() == false)
                            return;
                        if (item != null) {

                            item.setQty(Double.parseDouble(txtQty.getText().toString()));
                            item.setRate(Double.parseDouble(txtRate.getText().toString()));
                            item.setDiscPer(Double.parseDouble(txtDisc.getText().toString()));
                            if (item.getDiscPer() == 0) {
                                item.setDiscAmt(0);
                            }

                            if (IsItemDisplay == true) {
                                saleBillAdapter.UpdateItems(Pos, item);
                            } else {
                                saleBillAdapter.addOrderListItem(item);
                            }
                            saleBillAdapter.notifyDataSetChanged();
                            lblItemCount.setText("" + saleBillAdapter.getCount());
                            lblTotalAmount.setText("" + saleBillAdapter.GetTotalAmount());
                        }
                        dismiss();
                    } catch (Exception ex) {
                        ShowMessage(ex.getMessage());
                    }
                    break;
                case R.id.btn_ItemSearch_Cancel:
                    this.dismiss();
                    break;
                case R.id.img_SearchBarcode:
                    try {
                        Intent intent = new Intent(ACTION_SCAN);
                        intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
                        startActivityForResult(intent, 0);
                    } catch (ActivityNotFoundException anfe) {
                        showDialog(getActivity(), "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
                    }
                    break;
                default:
                    break;
            }
            //
        }

        private boolean Validation() {

            if (item == null) {
                txtItemsSarch.requestFocus();
                txtItemsSarch.setError("Please Search the Item");
                return false;

            }
            if (txtQty.getText().toString().isEmpty()) {
                txtQty.requestFocus();
                txtQty.setError("Enter Qty");
                return false;
            }
            if (txtRate.getText().toString().isEmpty()) {
                txtRate.requestFocus();
                txtRate.setError("Enter Rate");
                return false;
            }
            if (txtDisc.getText().toString().isEmpty()) {
                txtRate.requestFocus();
                txtRate.setError("Enter Discount %");
                return false;
            }
            return true;
        }

        public void SearchItems(Item item) {
            TextView lblMRP, lblUom, lblBarcode;
            lblMRP = (TextView) findViewById(R.id.lbl_ItemSearch_MRP);
            lblUom = (TextView) findViewById(R.id.lbl_ItemSearch_UOM);
            lblBarcode = (TextView) findViewById(R.id.lbl_ItemSearch_Barcode);

            lblMRP.setText("" + item.getMrp());
            lblUom.setText(item.getUOM());
            lblBarcode.setText(item.getBarcode());
            txtItemsSarch.setText(item.getItemNameDisp());
            txtRate.setText("" + item.getRate());
            this.item = item;
            txtQty.requestFocus();

            if (IsItemDisplay == true) {
                txtItemsSarch.setText(item.getItemNameDisp());
                txtQty.setText("" + item.getQty());
                txtDisc.setText("" + item.getDiscPer());


            }

        }

        private AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
            AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
            downloadDialog.setTitle(title);
            downloadDialog.setMessage(message);
            downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        act.startActivity(intent);
                    } catch (ActivityNotFoundException anfe) {

                    }
                }
            });
            downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            return downloadDialog.show();
        }


    }

    @SuppressLint("ValidFragment")
    public class SelectDateFragmentFrmdate extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy2, int mmto2, int dd2) {
            populateSetDate(yy2, mmto2, dd2, true, view);

        }
    }


    //endregion

    //region Save Bill Related Methods
    public class Custom_SaveBill extends Dialog implements
            android.view.View.OnClickListener {
        public Activity c;
        public Dialog d;
        public Button btnCancel, BtnDate, btnSave;
        Spinner DDMLedger, DDPayType;
        TextView lblTotalAmount, lblSubAmount, lblBillNo;
        double BillAmount, SumAmount;
        EditText txtDiscAmt, txtDiscPer;
        int pos_cust = -1, pos_PayType = -1;
        CustomerSerach_Adapter ad_Cudtomer;
        List<UCLedger> mLedger = new ArrayList<>();
        AutoCompleteTextView txtCustomer;

        public Custom_SaveBill(Activity a) {
            super(a);
            this.c = a;
            ad_Cudtomer = new CustomerSerach_Adapter(a, R.layout.activity_main, mLedger);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            this.setCancelable(false);
            this.setTitle("Save Bill");
            setContentView(R.layout.dialog_billsave);

            DDMLedger = (Spinner) findViewById(R.id.cmb_savebill_MLedger);
            DDPayType = (Spinner) findViewById(R.id.cmb_savebill_PayType);
            lblSubAmount = (TextView) findViewById(R.id.lbl_sabebill_SubAmount);
            lblTotalAmount = (TextView) findViewById(R.id.lbl_salebill_GAmount);
            lblBillNo = (TextView) findViewById(R.id.lbl_sabebill_InvNo);
            BtnDate = (Button) findViewById(R.id.txt_savebill_Date);
            btnSave = (Button) findViewById(R.id.btn_savebill_Save);
            btnCancel = (Button) findViewById(R.id.btn_savebill_Cancel);
            txtDiscAmt = (EditText) findViewById(R.id.txt_sabebill_DiscAmount);
            txtDiscPer = (EditText) findViewById(R.id.txt_sabebill_DiscPer);

            //  DDMLedger.setAdapter(ad_Customer);
            DDPayType.setAdapter(ad_PayType);


            BtnDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SetDate(BtnDate);
                }
            });
            btnSave.setOnClickListener(this);
            btnCancel.setOnClickListener(this);
            DDMLedger.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    // pos_cust = ((DDLEntity) ad_Customer.getItem(position)).getID();
                    pos_cust = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub
                }
            });
            DDPayType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    //pos_PayType = ((DDLEntity) ad_PayType.getItem(position)).getID();
                    pos_PayType = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub
                }
            });

            txtDiscAmt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        double DiscPer = (100 * Double.parseDouble(txtDiscAmt.getText().toString())) / SumAmount;
                        BillAmount = SumAmount - Double.parseDouble(txtDiscAmt.getText().toString());
                        txtDiscPer.setText("" + DiscPer);
                        lblTotalAmount.setText("" + BillAmount);
                    }
                }
            });

            txtDiscAmt.addTextChangedListener(new TextWatcher() {

                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    if (s.length() > 0) {
                        double DiscPer = (100 * Double.parseDouble(s.toString())) / SumAmount;
                        BillAmount = SumAmount - Double.parseDouble(s.toString());
                        txtDiscPer.setText("" + DiscPer);
                        lblTotalAmount.setText("" + BillAmount);
                    }

                }

                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                public void afterTextChanged(Editable s) {

                }
            });

            txtDiscPer.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        double DiscAmt = (SumAmount * Double.parseDouble(txtDiscPer.getText().toString())) / 100;
                        BillAmount = SumAmount - DiscAmt;
                        txtDiscAmt.setText("" + DiscAmt);
                        lblTotalAmount.setText("" + BillAmount);
                    }
                }
            });

            txtCustomer = (AutoCompleteTextView) findViewById(R.id.ac_CustomerSearch);
            txtCustomer.setAdapter(ad_Cudtomer);
            txtCustomer.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
                    pos_cust = pos;
                }

            });

            BillAmount = SumAmount = saleBillAdapter.GetTotalAmount();
            lblTotalAmount.setText("" + BillAmount);
            lblSubAmount.setText("" + BillAmount);
            if (PkVoucherNo == 0) {
                long BillNo = cc.ReturnLong("Select  Max(VoucherUserNo) From TVoucherEntry ");
                lblBillNo.setText("" + (BillNo + 1));
                BtnDate.setText(cc.GetCurrentDate());
                txtCustomer.requestFocus();
            } else {
                //Fill Control
                lblBillNo.setText("" + tvch.VoucherUserNo);
                BtnDate.setText(tvch.VoucherDate);
                pos_PayType = ad_PayType.GetIndex(tvch.PayTypeNo, ad_PayType);
                DDPayType.setSelection(pos_PayType);
                txtDiscAmt.setText("" + tvch.DiscAmt);
                txtCustomer.setText(cc.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + tvch.LedgerNo + ""));
                pos_cust = GetIndex(tvch.LedgerNo, ad_Cudtomer);
                btnSave.requestFocus();
            }
            //  DDMLedger.setFocusable(true);
            // DDMLedger.setFocusableInTouchMode(true);

        }

        public int GetIndex(long ID, CustomerSerach_Adapter adpter) {
            int Value = 0;
            for (int position = 0; position < adpter.getCount(); position++) {
                if (adpter.getItem(position).LedgerNo == ID) {
                    Value = position;
                    break;
                }
            }
            return Value;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.btn_savebill_Cancel:
                    this.dismiss();
                    break;

                case R.id.btn_savebill_Save:

                    if (pos_cust == -1) {
                        txtCustomer.requestFocus();
                        txtCustomer.setError("Select Customer Name");
                        return;
                    }
                    long recCnt = cc.ReturnLong("Select Count(*) From TVoucherRefDetails Where RefNo=" + PkVoucherNo + " And TypeOfRef=2");
                    if (recCnt != 0) {
                        ShowMessage("Already this bill is amount collected");
                        return;
                    }
                    SaveBill();
                    break;

            }

        }


        private void SaveBill() {
            dbtVoucherEntry = new DBTVoucherEntry();
            if (PkVoucherNo != 0) {
                cc.execSQL("Delete From TStock Where FkvoucherNo=" + PkVoucherNo + "");
                cc.execSQL("Delete From TVoucherRefDetails Where RefNo=" + PkVoucherNo + "");
            }

            long ID = 0;
            try {
                tvch = new UCSaleBills().new UCTVoucherEntry();
                tvch.PkVoucherNo = 0;
                tvch.LedgerNo = ad_Cudtomer.getItemId(pos_cust);
                tvch.PayTypeNo = ad_PayType.getItem(pos_PayType).getID();
                tvch.BilledAmount = BillAmount;
                tvch.DiscAmt = (SumAmount - BillAmount);
                tvch.ChargAmount = 0;
                tvch.IsCancel = false;
                tvch.RateTypeNo = 2;
                tvch.Remark = "Mobile Sale Bill";
                tvch.MobileBillNo = Long.parseLong(lblBillNo.getText().toString());
                tvch.StatusNo = "0";
                tvch.VoucherDate = cc.GetCurrentDate_VoucherDate(BtnDate.getText().toString());
                tvch.VoucherTime = cc.GetCurrentDate();
                tvch.VoucherTypeCode = 15;
                tvch.VoucherUserNo = Long.parseLong(lblBillNo.getText().toString());
                if (PkVoucherNo == 0) {
                    ID = cc.createEntity(dbtVoucherEntry.tableName, dbtVoucherEntry.AddTVoucherEntry(tvch));
                } else {
                    if (cc.UpdateEntity(dbtVoucherEntry.tableName, dbtVoucherEntry.AddTVoucherEntry(tvch), "PkSrNo=?", "" + PkVoucherNo) == 1)
                        ID = PkVoucherNo;
                }

                if (ID == (long) -1) {
                    throw new RuntimeException("Error In TVoucherEntry to save");
                }

                if (ad_PayType.getItem(pos_PayType).getID() == 2) {
                    DBTVoucherRefDetails dbtVoucherRefDetails = new DBTVoucherRefDetails();
                    UCSaleBills.UCTVoucherRefDetails tvchRef = new UCSaleBills().new UCTVoucherRefDetails();

                    tvchRef.Amount = BillAmount;
                    tvchRef.Balamount = BillAmount;
                    tvchRef.CardNo = "";
                    tvchRef.CheqDate = "";
                    tvchRef.DiscAmount = 0;
                    tvchRef.FkvoucherNo = 0;
                    tvchRef.LedgerNo = tvch.LedgerNo;
                    tvchRef.PayDate = "";
                    tvchRef.PayType = (tvch.PayTypeNo == 1) ? "Cash" : "Credit";
                    tvchRef.Refno = ID;
                    tvchRef.TypeofRef = 3;
                    tvchRef.Remark = "";

                    long TRID = cc.createEntity(dbtVoucherRefDetails.tableName, dbtVoucherRefDetails.AddTVoucherRefDetails(tvchRef));
                    if (TRID == (long) -1) {
                        throw new RuntimeException("Error In TVoucherRefDetails to save");
                    }

                }

                for (int i = 0; i < saleBillAdapter.getCount(); i++) {
                    dbtStock = new DBTStock();
                    UCSaleBills.UCTStock tstock = new UCSaleBills().new UCTStock();
                    Item item = saleBillAdapter.getItem(i);

                    tstock.PkvouchertrnNo = 0;
                    tstock.Amount = item.getAmount();
                    tstock.Barcode = item.getBarcode();
                    tstock.CESSAmt = 0;
                    tstock.CESSPer = 0;
                    tstock.CGSTAmt = 0;
                    tstock.CGSTPer = 0;
                    tstock.IGSTAmt = 0;
                    tstock.IGSTPer = 0;
                    tstock.DiscAmount = item.getDiscAmt();
                    tstock.HSNCode = "";
                    tstock.FkvoucherNo = ID;
                    tstock.Itemname = item.getItemNameDisp();
                    tstock.ItemNo = item.getItemNo();
                    tstock.Mrp = item.getMrp();
                    tstock.NetAmount = (item.getNetRate() == 0) ? 0 : (item.getNetRate() * item.getQty());
                    tstock.NetRate = item.getNetRate();
                    tstock.Qty = item.getQty();
                    tstock.Rate = item.getRate();
                    tstock.TaxAmount = 0;
                    tstock.Taxpercentage = 0;
                    tstock.UomName = item.getUOM();
                    tstock.LangitemName = "";
                    tstock.TempItemNo = item.getPkSrNo();
                    long TID = cc.createEntity(dbtStock.tableName, dbtStock.AddTStock(tstock));
                    if (TID == (long) -1) {
                        throw new RuntimeException("Error In TStock to save");
                    }
                }


                ShowMessage("Bill No " + tvch.VoucherUserNo + " Added Successfully");
                ((SaleBillActivity) getActivity()).displayView(0, 0, null);
                this.dismiss();
            } catch (Exception ex) {

                Log.e(Tag, ex.getMessage().toString());
                cc.execSQL("Delete From TStock Where FkvoucherNo=" + ID + "");
                cc.execSQL("Delete From TVoucherRefDetails Where FkvoucherNo=" + ID + "");
                cc.execSQL("Delete From TVoucherEntry Where PkSrNo=" + ID + "");

                ShowMessage("Bill Not Added Successfully");
            }
        }


    }


    //endregion
}