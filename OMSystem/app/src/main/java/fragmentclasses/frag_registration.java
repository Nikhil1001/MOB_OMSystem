package fragmentclasses;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.provider.Settings.Secure;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.om.omsystem.HomeActivity;
import com.om.omsystem.R;

import java.lang.reflect.Type;

import DBClasses.CommonClass;
import DBClasses.DBMRegistration;
import Web.WebSevice;
import entity.UCRegistration;

public class frag_registration extends Fragment implements View.OnClickListener {

    EditText txtName, txtMobile, txtEmailID, txtUserName, txtPassword, txtStoreUserName, txtStorePassword;
    Button btnSave, btnCancel;
    CommonClass cc = new CommonClass();
    String IpAddress = "www.rshopnow.com";
    DBMRegistration dbmRegistration = new DBMRegistration();
    String Tag = "frag_registration :";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View rootView = inflater.inflate(R.layout.fragment_registration,
                container, false);

        txtName = (EditText) rootView.findViewById(R.id.txt_reg_Name);
        txtEmailID = (EditText) rootView.findViewById(R.id.txt_reg_EmailID);
        txtMobile = (EditText) rootView.findViewById(R.id.txt_reg_MobileNo);
        txtUserName = (EditText) rootView.findViewById(R.id.txt_reg_UserName);
        txtPassword = (EditText) rootView.findViewById(R.id.txt_reg_Password);
        btnSave = (Button) rootView.findViewById(R.id.btn_reg_save);
        btnCancel = (Button) rootView.findViewById(R.id.btn_reg_Cancel);
        txtStoreUserName = (EditText) rootView.findViewById(R.id.txt_reg_StoreUserName);
        txtStorePassword = (EditText) rootView.findViewById(R.id.txt_reg_StorePassword);


        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        return rootView;
    }


    @Override
    public void onClick(View v) {
        // default method for handling onClick Events..
        switch (v.getId()) {
            case R.id.btn_reg_save:
                UCRegistration items = new UCRegistration();
                items.UserName = txtName.getText().toString();
                items.MobileNo = txtMobile.getText().toString();
                items.UserID = txtUserName.getText().toString();
                items.Password = txtPassword.getText().toString();
                items.Email_ID = txtEmailID.getText().toString();
                items.DeviceID = Secure.getString(getActivity().getContentResolver(),
                        Secure.ANDROID_ID);
                items.RegDate = cc.GetDateAndTime();
                items.IPAdress = IpAddress;
                items.StoreEmail_ID = txtStoreUserName.getText().toString();
                items.StorePassword = txtStorePassword.getText().toString();
                items.CompanyName = "";
                CommonClass.DBGetVal.IpAddress = IpAddress;

                Gson gs = new Gson();
                String Str = gs.toJson(items).toString();
                Log.i(Tag, Str);
                AsyncTaskRunner asc = new AsyncTaskRunner();
                asc.execute(Str);
                break;

        }
    }

    public void ShowMessage(String StrValue, long Type) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
        dlgAlert.setMessage(StrValue);
        dlgAlert.setTitle("Registration");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
        if (Type == 1) {
            ((HomeActivity) getActivity()).UpdateSetting();
            ((HomeActivity) getActivity()).displayView(HomeActivity.Fragment_Login, -1);
        }
        //   ((HomeActivity) getActivity()).displayView(HomeActivity.Fragment_Welcome, -1);
        // else
        //   ((HomeActivity) getActivity()).displayView(HomeActivity.Fragment_CrimeStatus, -1);
    }


    private class AsyncTaskRunner extends AsyncTask<String, Integer, String> {

        ProgressDialog progressDialog;
        private String resp, StrException = null;



        @Override
        protected String doInBackground(String... params) {
            try {
                return WebSevice.PostRequest("/Registration", params[0]);
            } catch (Exception ex) {
                StrException = ex.getMessage();
                return "";
            }
        }


        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            Log.i(Tag, result);
            if (result.equalsIgnoreCase("") == false) {
                Type token = new TypeToken<UCRegistration>() {
                }.getType();
                Gson gson = new Gson();
                UCRegistration lstItems = gson.fromJson(result, token);
                if (lstItems != null) {
                    lstItems.IPAdress = CommonClass.DBGetVal.IpAddress;
                    boolean flag = dbmRegistration.AddMRegistration(lstItems);
                    if (flag == true) {
                        ShowMessage("Registration Details Save Successfully..", 1);

                    } else
                        ShowMessage("Registration Details Not Save", 0);
                }
            } else if (StrException.equalsIgnoreCase("") == false) {
                ShowMessage(StrException, 0);
            }
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Processing Request...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }
    }
}