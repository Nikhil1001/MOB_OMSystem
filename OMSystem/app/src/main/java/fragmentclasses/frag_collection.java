package fragmentclasses;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.om.omsystem.HomeActivity;
import com.om.omsystem.R;
import com.om.omsystem.SaleBillActivity;

import java.util.ArrayList;
import java.util.Calendar;

import CustomDialog.CustomDropDialogClass;
import DBClasses.CommonClass;
import DBClasses.DBTVoucherEntry;
import DBClasses.DBTVoucherRefDetails;
import adapter.CollectionBillAdapter;
import adapter.DropDownAdapter;
import adapter.SearchBillAdapter;
import entity.DDLEntity;
import entity.DTOCollectionBill;
import entity.SearchBill;
import entity.UCSaleBills;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class frag_collection extends Fragment implements View.OnClickListener {


    public int pos_cust = 0;
    Calendar calendar = Calendar.getInstance();
    int yy = calendar.get(Calendar.YEAR);
    int mm = calendar.get(Calendar.MONTH);
    int dd = calendar.get(Calendar.DAY_OF_MONTH);
    CommonClass cc = new CommonClass();
    Button btnSave;
    ImageView img_Back;
    TextView TxtDate;
    ListView lstDetails;
    EditText txtAmount;
    String Tag = "frag_Search_Bill";
    Spinner DDMLedger;
    DropDownAdapter ad_Customer;
    TextView cmb_Collection_MLedger1;


    CollectionBillAdapter searchBillAdapter;
    TextView lblBillAmt, lblRecAmt, lblNetBal;

    long LegderNo = 0;
    String LedgerName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_collection,
                container, false);
        setRetainInstance(true);

        btnSave = (Button) rootView.findViewById(R.id.btn_collection_Save);
        btnSave.setOnClickListener(this);

        TxtDate = (TextView) rootView.findViewById(R.id.btn_collection_FromDate);
        TxtDate.setOnClickListener(this);
        lstDetails = (ListView) rootView.findViewById(R.id.lv_collection_Details);
        searchBillAdapter = new CollectionBillAdapter();
        searchBillAdapter.setContext(getActivity());
        lstDetails.setAdapter(searchBillAdapter);

        lstDetails.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long id) {


                return true;
            }
        });


        img_Back = (ImageView) rootView.findViewById(R.id.img_Collection_Bill_toolbarHomeIv);
        img_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((HomeActivity) getActivity()).displayView(HomeActivity.Fragment_Menu, -1);
            }
        });
        TxtDate.setText(cc.GetCurrentDate());
        txtAmount = (EditText) rootView.findViewById(R.id.txt_collection_ColAmount);

        final ArrayList<DDLEntity> STATUS_LIST = cc
                .GetDropDownList_NoSelect("Select LedgerNo,LedgerName From MLedger order by LedgerName");
        ad_Customer = new DropDownAdapter(getActivity(),
                android.R.layout.simple_expandable_list_item_1, STATUS_LIST,
                getResources());
        DDMLedger = (Spinner) rootView.findViewById(R.id.cmb_Collection_MLedger);
        DDMLedger.setAdapter(ad_Customer);

        DDMLedger.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // pos_cust = ((DDLEntity) ad_Customer.getItem(position)).getID();
                pos_cust = position;
                ShowData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


        lblBillAmt = (TextView) rootView.findViewById(R.id.lbl_footer_BillAmt);
        lblRecAmt = (TextView) rootView.findViewById(R.id.lbl_footer_RecAmt);
        lblNetBal = (TextView) rootView.findViewById(R.id.lbl_footer_NetBal);

        cmb_Collection_MLedger1 = (TextView) rootView.findViewById(R.id.cmb_Collection_MLedger1);
        cmb_Collection_MLedger1.setText("---select---");
        cmb_Collection_MLedger1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CustomDropDialogClass cd = new CustomDropDialogClass(getActivity(), "Select Customer", STATUS_LIST, new CustomDropDialogClass.OnDailogDimmis() {
                    @Override
                    public void OnIdSelect(long ID, String Name) {
                        LegderNo = ID;
                        LedgerName = Name;
                        cmb_Collection_MLedger1.setText(Name);
                        ShowData();
                    }
                });
                cd.show();
            }
        });


        return rootView;
    }

    @Override
    public void onClick(View v) {
        // default method for handling onClick Events..
        switch (v.getId()) {
            case R.id.btn_collection_Save:

                try {
                    hideSoftKeyboard();

                    //region Validation

                    if (ad_Customer.getItem(pos_cust).getID() == 0) {
                        ShowMessage("Please Select the Cutomer Name.");
                        return;
                    }
                    if (searchBillAdapter.getCount() == 0) {
                        ShowMessage("No Bill Pending for Collection.");
                        return;
                    }
                    if (txtAmount.getText().toString().equalsIgnoreCase("") == true) {
                        txtAmount.setError("Please Enter Amount");
                        return;
                    }
                    if (Double.parseDouble(txtAmount.getText().toString()) > searchBillAdapter.getNetBal()) {
                        txtAmount.setError("Enter Amount less than Net BalAmount");
                        return;
                    }
                    //endregion

                    //region Save Collection Method
                    double Amount = Double.parseDouble(txtAmount.getText().toString());
                    for (DTOCollectionBill items : searchBillAdapter.GetOrderList()) {
                        if (items.NetBal <= Amount) {
                            items.CollectionAmt = items.NetBal;
                            Amount = Amount - items.NetBal;
                        } else if (Amount != 0) {
                            if (items.NetBal <= Amount) {
                                items.CollectionAmt = items.NetBal;
                                Amount = Amount - items.NetBal;
                            } else {
                                items.CollectionAmt = Amount;
                                Amount = 0;
                            }

                        } else {
                            items.CollectionAmt = (double) 0;
                        }
                    }

                    for (DTOCollectionBill items : searchBillAdapter.GetOrderList()) {
                        if (items.CollectionAmt == (double) 0)
                            continue;
                        DBTVoucherRefDetails dbtVoucherRefDetails = new DBTVoucherRefDetails();
                        UCSaleBills.UCTVoucherRefDetails tvrf = new UCSaleBills().new UCTVoucherRefDetails();
                        tvrf.PkreftrnNo = 0;
                        tvrf.FkvoucherNo = 0;
                        //tvrf.LedgerNo = ad_Customer.getItem(pos_cust).getID();
                        tvrf.LedgerNo = LegderNo;
                        tvrf.Refno = items.FkVoucherNo;
                        tvrf.TypeofRef = 2;//For Collection
                        tvrf.Amount = items.CollectionAmt;
                        tvrf.DiscAmount = 0;
                        tvrf.Balamount = 0;
                        tvrf.PayDate = cc.GetCurrentDate_VoucherDate(TxtDate.getText().toString());
                        tvrf.PayType = "CASH";
                        tvrf.CardNo = "";
                        tvrf.Remark = "Total Amount Collection :" + txtAmount.getText().toString() + "";
                        tvrf.CheqDate = "";
                        long TID = cc.createEntity(dbtVoucherRefDetails.tableName, dbtVoucherRefDetails.AddTVoucherRefDetails(tvrf));
                        if (TID == (long) -1) {
                            throw new RuntimeException("Error In tVoucherRefDetails to save");
                        } else {
                            boolean flag = cc.execSQL("Update TVoucherRefDetails Set Balamount=Balamount-" + items.CollectionAmt + " Where PkSrNo=" + items.PkSrNo + "");
                            if (flag == false) {
                                cc.execSQL("Delete From TVoucherRefDetails Where PkSrNo=" + TID + " ");
                                throw new RuntimeException("Error In tVoucherRefDetails to save");
                            }
                            cc.execSQL("Update TVoucherEntry Set StatusNo=0 Where PkSrNo=" + items.FkVoucherNo + " ");
                        }

                    }
                    ShowMessage("Collection Save successfully.");
                    ShowData();
                    txtAmount.setText("");
                    //endregion

                } catch (Exception ex) {
                    ShowMessage(ex.getMessage().toString());
                }
                break;
            case R.id.btn_search_bill_FromDate:
                SetDate();
                break;

        }
    }

    public void ShowMessage(String StrMess) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());

        dlgAlert.setMessage(StrMess);
        dlgAlert.setTitle("Error Message...");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void ShowData() {
        searchBillAdapter.removeAll();
        lblBillAmt.setText("0.00");
        lblRecAmt.setText("0.00");
        lblNetBal.setText("0.00");
        String str = "Select TE.SaleBillNo,TE.VoucherDate,TR.Amount,(TR.Amount-TR.BalAmount) AS RecAmt,TR.BalAmount,TR.RefNo,TR.PkSrNo " +
                " From TVoucherRefDetails AS TR Inner Join TVoucherEntry AS TE ON TR.REFNo =TE.PkSrNo   " +
                " Where TR.TypeOfRef=3 And TR.BalAmount<>0 And TR.LedgerNo=" + LegderNo+ " " +
                " order by Datetime(TE.VoucherDate) ";
        //" Where DateTime(VoucherDate)=DateTime('" + TxtFromDate.getText().toString() + "') ";
        Cursor cursor = cc.GetDataView(str);
        if (cursor.getCount() == 0)
            return;

        while (cursor.moveToNext()) {

            DTOCollectionBill tvc = new DTOCollectionBill();
            tvc.BillNo = "" + cursor.getString(0);
            tvc.BillDate = cursor.getString(1);
            tvc.BillAmount = cursor.getDouble(2);
            tvc.TotRecAmt = cursor.getDouble(3);
            tvc.NetBal = cursor.getDouble(4);
            tvc.FkVoucherNo = cursor.getLong(5);
            tvc.PkSrNo = cursor.getLong(6);
            searchBillAdapter.addOrderListItem(tvc);
        }
        searchBillAdapter.notifyDataSetChanged();

        if (searchBillAdapter.GetOrderList().size() != 0) {


            double BillAmt = 0, RecAmt = 0, NetBal = 0;
            for (DTOCollectionBill items : searchBillAdapter.GetOrderList()) {
                BillAmt = BillAmt + items.BillAmount;
                RecAmt = RecAmt + items.TotRecAmt;
                NetBal = NetBal + items.NetBal;
            }
            lblBillAmt.setText("" + BillAmt);
            lblRecAmt.setText("" + RecAmt);
            lblNetBal.setText("" + NetBal);
        }
    }

    //region Date Dialog

    public void SetDate() {
        DialogFragment newFragment;
        newFragment = new SelectDateFragmentFrmdate();
        newFragment.show(getActivity().getFragmentManager(),
                "Datepicker");
    }

    public void populateSetDate(int year, int month, int day,
                                Boolean IsFromDate1, DatePicker dtview) {
        yy = year;

        dd = day;
        try {
            mm = month;
            TxtDate.setText(cc.formatDate(year, month, day));
        } catch (Exception e) {
            Log.e("Report", "Error : " + e.getMessage());
        }

    }

    @SuppressLint("ValidFragment")
    public class SelectDateFragmentFrmdate extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy2, int mmto2, int dd2) {
            populateSetDate(yy2, mmto2, dd2, true, view);

        }
    }


    //endregion


}
