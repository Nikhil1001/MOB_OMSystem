package fragmentclasses;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.om.omsystem.HomeActivity;
import com.om.omsystem.R;
import com.om.omsystem.SaleBillActivity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import DBClasses.CommonClass;
import DBClasses.DBMCity;
import DBClasses.DBMLedger;
import DBClasses.DBMStockItems;
import DBClasses.DBTVoucherEntry;
import DBClasses.DBTVoucherRefDetails;
import Web.WebSevice;
import adapter.Home_Menu_Adapter;
import entity.UCCity;
import entity.UCLedger;
import entity.UCRegistration;
import entity.UCSaleBills;
import entity.UCStockItems;
import entity.dto.MenuItem;


public class frag_home extends Fragment implements View.OnClickListener {

    CommonClass cc = new CommonClass();
    Button btnMaster, btnSaleBill, btnImportBill, btnExportBill, btnCollection;
    String Tag = "frag_home";


    GridView gridView;
    ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
    Home_Menu_Adapter home_menu_adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home,
                container, false);



       /* btnMaster = (Button) rootView.findViewById(R.id.btn_home_Master);
        btnMaster.setOnClickListener(this);

        btnSaleBill = (Button) rootView.findViewById(R.id.btn_home_SaleBill);
        btnSaleBill.setOnClickListener(this);

        btnImportBill = (Button) rootView.findViewById(R.id.btn_home_ImportBill);
        btnImportBill.setOnClickListener(this);

        btnExportBill = (Button) rootView.findViewById(R.id.btn_home_ExportBill);
        btnExportBill.setOnClickListener(this);

        btnCollection = (Button) rootView.findViewById(R.id.btn_home_Collection);
        btnCollection.setOnClickListener(this);*/
        Bitmap homeIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.billing);
        //Bitmap userIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.billing);
        menuItems.add(new MenuItem(BitmapFactory.decodeResource(this.getResources(), R.drawable.sales), "Bill"));
        menuItems.add(new MenuItem(BitmapFactory.decodeResource(this.getResources(), R.drawable.report), "Reports"));
        menuItems.add(new MenuItem(BitmapFactory.decodeResource(this.getResources(), R.drawable.master), "Master"));
        menuItems.add(new MenuItem(BitmapFactory.decodeResource(this.getResources(), R.drawable.receipt), "Collection"));
        menuItems.add(new MenuItem(BitmapFactory.decodeResource(this.getResources(), R.drawable.imp), "Import Bill"));
        menuItems.add(new MenuItem(BitmapFactory.decodeResource(this.getResources(), R.drawable.export), "Export Bill"));


        gridView = (GridView) rootView.findViewById(R.id.gridview_home);
        home_menu_adapter = new Home_Menu_Adapter(getActivity(), R.layout.view_home_menu, menuItems);
        gridView.setAdapter(home_menu_adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                //  Toast.makeText(getApplicationContext(), gridArray.get(position).getTitle(), Toast.LENGTH_SHORT).show();
                Call_Menu(position);

            }
        });


        return rootView;
    }

    private void Call_Menu(int Pos) {
        final DwonloadMaster dm = new DwonloadMaster();
        switch (Pos) {
            case 2://Mster IMaport
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Do you want to Export Master.?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dm.execute("1");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });
                builder.create().show();
                break;
            case 0://Bill Menu
                // ((HomeActivity) getActivity()).displayView(HomeActivity.Fragment_SaleBill, -1);
                Intent downloadIntent = new Intent(((HomeActivity) getActivity()), SaleBillActivity.class);
                Bundle b = new Bundle();
                b.putString("Bill_Menu", "0");//Bill
                downloadIntent.putExtras(b);
                startActivity(downloadIntent);
                ((HomeActivity) getActivity()).finish();
                break;

            case 1://Reports Menu
                // ((HomeActivity) getActivity()).displayView(HomeActivity.Fragment_SaleBill, -1);
                Intent Reports = new Intent(((HomeActivity) getActivity()), SaleBillActivity.class);
                Bundle b1 = new Bundle();
                b1.putString("Bill_Menu", "1");//Reports
                Reports.putExtras(b1);
                startActivity(Reports);
                ((HomeActivity) getActivity()).finish();
                break;
            case 4://ImportBill
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                builder1.setMessage("Do you want to Import Bill Data?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dm.execute("2");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });
                builder1.create().show();

                break;
            case 5://ExportBill
                AlertDialog.Builder builder3 = new AlertDialog.Builder(getActivity());
                builder3.setMessage("Do you want to Export Bill.?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dm.execute("3");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });
                builder3.create().show();
                break;
            case 3://Collection
                ((HomeActivity) getActivity()).displayView(HomeActivity.Fragment_Collection, -1);
                break;


        }
    }

    @Override
    public void onClick(View v) {
        // default method for handling onClick Events..


    }

    public void ShowMessage(String StrMess) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());

        dlgAlert.setMessage(StrMess);
        dlgAlert.setTitle("Error Message...");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    private class DwonloadMaster extends AsyncTask<String, Integer, String> {

        ProgressDialog progressDialog;
        private String resp;

        @Override
        protected String doInBackground(String... params) {
            //  return WebSevice.GetServiceData_Trans(params[0]);
            String MSG = "";
            try {

                switch (params[0].toString()) {
                    case "1":
                        //region Download All Master
                        //Download City
                        String result = WebSevice.GetRequest("/City");
                        if (result.equalsIgnoreCase("") == false) {
                            Type token = new TypeToken<List<UCCity>>() {
                            }.getType();
                            Gson gson = new Gson();
                            List<UCCity> lstItems = gson.fromJson(result, token);
                            if (lstItems != null) {
                                cc.execSQL("Delete From MCity");
                                DBMCity dbmCity = new DBMCity();
                                dbmCity.SaveCity(lstItems);
                            }
                        }
                        //Download StockItems
                        result = WebSevice.GetRequest("/StockItems");
                        if (result.equalsIgnoreCase("") == false) {
                            Type token = new TypeToken<List<UCStockItems>>() {
                            }.getType();
                            Gson gson = new Gson();
                            List<UCStockItems> lstItems = gson.fromJson(result, token);
                            if (lstItems != null) {
                                cc.execSQL("Delete From MStockItems");
                                DBMStockItems dbmStockItems = new DBMStockItems();
                                dbmStockItems.Save_MStockItems(lstItems);
                            }
                        }

                        //Download Customer
                        result = WebSevice.GetRequest("/Customer");
                        if (result.equalsIgnoreCase("") == false) {
                            Type token = new TypeToken<List<UCLedger>>() {
                            }.getType();
                            Gson gson = new Gson();
                            List<UCLedger> lstItems = gson.fromJson(result, token);
                            if (lstItems != null) {
                                cc.execSQL("Delete From MLedger");
                                DBMLedger dbmLedger = new DBMLedger();
                                dbmLedger.Save_MLedger(lstItems);
                            }
                        }

                        //endregion
                        MSG = "Download Master successfully...";
                        break;
                    case "2":
                        //Download Sale Bill

                        result = WebSevice.GetRequest("/Bill");
                        Log.i(Tag, result);
                        if (result.equalsIgnoreCase("") == false) {
                            Type token = new TypeToken<UCSaleBills.UCSaleBillsList>() {
                            }.getType();
                            Gson gson = new Gson();
                            UCSaleBills.UCSaleBillsList lstItems = gson.fromJson(result, token);
                            if (lstItems != null) {
                                DBTVoucherEntry dbtVoucherEntry = new DBTVoucherEntry();
                                dbtVoucherEntry.SaveBill_Online(lstItems);
                            }
                        }
                        MSG = "Download Sale Bill successfully...";
                        break;
                    case "3"://Export Bill
                        //Upload Sale Bill
                        DBTVoucherEntry dbtVoucherEntry = new DBTVoucherEntry();
                        UCSaleBills.UCSaleBillsList lstSB = dbtVoucherEntry.GetBill_online();
                        if (lstSB.salesBills.size() != 0) {

                            UCSaleBills.UCSaleBillsList lstSB1 = new UCSaleBills().new UCSaleBillsList();
                            for (int i = 0; i < lstSB.salesBills.size(); i++) {
                                lstSB1.salesBills.add(lstSB.salesBills.get(i));
                                if (i % 3 == 0 && i > 0) {
                                    UploadSaleBill(lstSB1);
                                    lstSB1 = new UCSaleBills().new UCSaleBillsList();
                                }

                            }
                            if (lstSB1.salesBills.size() > 0) {
                                UploadSaleBill(lstSB1);
                            }


                        }
                        //Export the collection
                        List<UCSaleBills.UCTVoucherRefDetails> lstCollection = dbtVoucherEntry.GetCollection_online();
                        if (lstCollection.size() != 0) {
                            Gson gs = new Gson();
                            String Str = gs.toJson(lstCollection).toString();
                            Log.i(Tag, Str);
                            result = WebSevice.PostRequest("/Bill/Collection", Str);
                            if (result.equalsIgnoreCase("") == false) {

                                Type token = new TypeToken<List<DBTVoucherEntry.TReturnBill>>() {
                                }.getType();
                                Gson gson = new Gson();
                                List<DBTVoucherEntry.TReturnBill> lstItems = gson.fromJson(result, token);
                                for (DBTVoucherEntry.TReturnBill items : lstItems) {
                                    cc.execSQL("Update TVoucherRefDetails Set PkRefTrnNo=" + items.FkVoucherNo + " Where PkSrNo=" + items.PkSrNo + " ");
                                }

                            }
                        }
                        MSG = "Upload Bills successfully...";

                        break;

                }
            } catch (Exception ex) {
                return ex.getMessage();
            }
            return MSG;
        }


        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();

            Log.i(Tag, result);
            if (result.equalsIgnoreCase("") == false) {
                ShowMessage(result);
            }

        }

        private void UploadSaleBill(UCSaleBills.UCSaleBillsList lstSB1) {
            String result;
            Gson gs = new Gson();
            String Str = gs.toJson(lstSB1).toString();
            Log.i(Tag, Str);
            result = WebSevice.PostRequest("/Bill/SaveBill", Str);
            if (result.equalsIgnoreCase("") == false) {

                Type token = new TypeToken<List<DBTVoucherEntry.TReturnBill>>() {
                }.getType();
                Gson gson = new Gson();
                List<DBTVoucherEntry.TReturnBill> lstItems = gson.fromJson(result, token);
                for (DBTVoucherEntry.TReturnBill items : lstItems) {
                    cc.execSQL("Update TVoucherEntry Set StatusNo=2,PkVoucherNo=" + items.FkVoucherNo + " Where PkSrNo=" + items.PkSrNo + " ");
                }

            }
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Processing Request...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }
    }


}
