package fragmentclasses;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.om.omsystem.HomeActivity;
import com.om.omsystem.R;
import com.om.omsystem.SaleBillActivity;

import java.util.Calendar;

import DBClasses.CommonClass;
import adapter.SearchBillAdapter;
import entity.SearchBill;


public class frag_search_bill extends Fragment implements View.OnClickListener {


    Calendar calendar = Calendar.getInstance();
    int yy = calendar.get(Calendar.YEAR);
    int mm = calendar.get(Calendar.MONTH);
    int dd = calendar.get(Calendar.DAY_OF_MONTH);
    CommonClass cc = new CommonClass();


    Button btnShow;
    ImageView img_Back;
    TextView TxtFromDate, TxtToDate;
    ListView lstDetails;
    String Tag = "frag_Search_Bill";
    int DataType = 0;
    SearchBillAdapter searchBillAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_search_bill,
                container, false);
        setRetainInstance(true);
        btnShow = (Button) rootView.findViewById(R.id.btn_search_bill_Show);
        btnShow.setOnClickListener(this);
        TxtFromDate = (TextView) rootView.findViewById(R.id.btn_search_bill_FromDate);
        TxtFromDate.setOnClickListener(this);
        TxtToDate = (TextView) rootView.findViewById(R.id.btn_search_bill_ToDate);
        TxtToDate.setOnClickListener(this);
        lstDetails = (ListView) rootView.findViewById(R.id.lv_search_bill_Details);
        searchBillAdapter = new SearchBillAdapter();
        searchBillAdapter.setContext(getActivity());
        lstDetails.setAdapter(searchBillAdapter);
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.btn_Search_Bill_addNewBill);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SaleBillActivity) getActivity()).displayView(0, 0, null);

            }
        });

        img_Back = (ImageView) rootView.findViewById(R.id.img_Search_Bill_toolbarHomeIv);
        img_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent downloadIntent = new Intent(getActivity(), HomeActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("BackSarchBill", "SearchBill");
                downloadIntent.putExtras(bundle);
                startActivity(downloadIntent);
                getActivity().finish();

            }
        });


        lstDetails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                              @Override
                                              public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                                  Bundle bundle = new Bundle();
                                                  bundle.putLong("PkSrNo", searchBillAdapter.getItem(i).PkSrNo);
                                                  ((SaleBillActivity) getActivity()).displayView(0, 0, bundle);
                                              }
                                          }
        );
        TxtFromDate.setText("01/"+cc.GetCurrentDate("MMM/yyyy"));
        TxtToDate.setText(cc.GetCurrentDate());
       // ShowData();
        return rootView;
    }

    @Override
    public void onClick(View v) {
        // default method for handling onClick Events..
        switch (v.getId()) {
            case R.id.btn_search_bill_Show:
                ShowData();
                break;
            case R.id.btn_search_bill_FromDate:
                DataType = 1;
                SetDate();
                break;
            case R.id.btn_search_bill_ToDate:
                DataType = 2;
                SetDate();
                break;

        }
    }

    public void ShowMessage(String StrMess) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());

        dlgAlert.setMessage(StrMess);
        dlgAlert.setTitle("Error Message...");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    private void ShowData() {
        searchBillAdapter.removeAll();
        String str = " Select (Case When( TV.VoucherUserNo=0) then saleBillNo Else TV.VoucherUserNo End),ML.LedgerName,TV.VoucherDate,TV.BillAmount,TV.PkSrNo " +
                " From TVoucherEntry as TV Inner Join MLedger ML on ML.LedgerNo=TV.LedgerNo " +
                " Where TV.VoucherUserNo<>0  And "+
                " DateTime(TV.VoucherDate)>=DateTime('" + cc.GetCurrentDate_VoucherDate(TxtFromDate.getText().toString()) + "') And "+
                " DateTime(TV.VoucherDate)<=DateTime('" + cc.GetCurrentDate_VoucherDate(TxtToDate.getText().toString()) + "')";

        //" Where DateTime(VoucherDate)=DateTime('" + TxtFromDate.getText().toString() + "') ";
        Cursor cursor = cc.GetDataView(str);
        if (cursor.getCount() == 0)
            return;

        while (cursor.moveToNext()) {

            SearchBill searchBill = new SearchBill();
            searchBill.BillNo = "" + cursor.getLong(0);
            searchBill.LedgerName = cursor.getString(1);
            searchBill.BillDate = cc.SetCurrentDate_VoucherDate(cursor.getString(2));
            searchBill.BillAmount = cursor.getDouble(3);
            searchBill.PkSrNo = cursor.getLong(4);
            searchBillAdapter.addOrderListItem(searchBill);
        }
        searchBillAdapter.notifyDataSetChanged();
    }

    //region Date Dialog

    public void SetDate() {
        DialogFragment newFragment;
        newFragment = new SelectDateFragmentFrmdate();
        newFragment.show(getActivity().getFragmentManager(),
                "Datepicker");
    }

    @SuppressLint("ValidFragment")
    public class SelectDateFragmentFrmdate extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy2, int mmto2, int dd2) {
            populateSetDate(yy2, mmto2, dd2, true, view);

        }
    }

    public void populateSetDate(int year, int month, int day,
                                Boolean IsFromDate1, DatePicker dtview) {
        yy = year;

        dd = day;
        try {
            mm = month;
            if (DataType == (int) 1)
                TxtFromDate.setText(cc.formatDate(year, month, day));
            else
                TxtToDate.setText(cc.formatDate(year, month, day));
        } catch (Exception e) {
            Log.e("Report", "Error : " + e.getMessage());
        }

    }


    //endregion


}
