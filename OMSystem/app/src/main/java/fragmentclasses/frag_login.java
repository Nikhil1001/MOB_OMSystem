package fragmentclasses;

import com.om.omsystem.HomeActivity;
import com.om.omsystem.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Type;

import DBClasses.CommonClass;
import DBClasses.DBMRegistration;
import Web.WebSevice;
import entity.UCRegistration;
import DBClasses.CommonClass.DBGetVal;

public class frag_login extends Fragment implements View.OnClickListener {

    public frag_login() {
    }

    long LoginType;
    Button btnLogin, btnCancel;
    EditText txtUserName, txtPassword;
    TextView lblHeadetTaex;
    String Tag = "frag_login :";
    DBMRegistration dbmRegistration = new DBMRegistration();
    CommonClass cc = new CommonClass();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_login,
                container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            LoginType = bundle.getLong("LoginType", 0);
        }

        btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        btnCancel = (Button) rootView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);

        txtUserName = (EditText) rootView.findViewById(R.id.txtUsername);
        txtPassword = (EditText) rootView.findViewById(R.id.txtPassword);
        lblHeadetTaex = (TextView) rootView.findViewById(R.id.lbl_login_LoginTital);

        lblHeadetTaex.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showChangeLangDialog();
                return true;
            }
        });

        return rootView;
    }

    public void showChangeLangDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_ipaddress, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.txt_dialong_Ipaddress);
        edt.setText(DBGetVal.IpAddress);
        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                cc.execSQL("Update  MRegistration Set  IPAdress='" + edt.getText() + "' ");
                DBGetVal.IpAddress = edt.getText().toString();
                dialog.dismiss();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public void onClick(View v) {
        // default method for handling onClick Events..
        switch (v.getId()) {
            case R.id.btnLogin:
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                if (txtUserName.getText().toString().equalsIgnoreCase("") == true) {
                    ShowMessage("Enter User Name");
                } else if (txtPassword.getText().toString().equalsIgnoreCase("") == true) {
                    ShowMessage("Enter Password");
                } else {
                    if (DBGetVal.IsActive == 1) {
                        if (txtUserName.getText().toString().equalsIgnoreCase(DBGetVal.UserName) == false)
                            ShowMessage("User Name is Incorrect");
                        else if (txtPassword.getText().toString().equalsIgnoreCase(DBGetVal.Password) == false)
                            ShowMessage("Password Incorrect");
                        else
                            ((HomeActivity) getActivity()).displayView(HomeActivity.Fragment_Menu, -1);

                    } else {
                        AsyncTaskRunner ask = new AsyncTaskRunner();
                        ask.execute("/Registration");
                    }
                }
                break;
            case R.id.btnCancel:
                getActivity().finish();
                break;
        }

    }

    public void ShowMessage(String StrMess) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());

        dlgAlert.setMessage(StrMess);
        dlgAlert.setTitle("Error Message...");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    private class AsyncTaskRunner extends AsyncTask<String, Integer, String> {

        private String resp;
        ProgressDialog progressDialog;


        @Override
        protected String doInBackground(String... params) {
            //  return WebSevice.GetServiceData_Trans(params[0]);
            String Str = params[0].toString();
            return WebSevice.GetRequest(Str);
        }


        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();

            Log.i(Tag, result);
            if (result.equalsIgnoreCase("") == false) {
                Type token = new TypeToken<UCRegistration>() {
                }.getType();
                Gson gson = new Gson();
                UCRegistration lstItems = gson.fromJson(result, token);


                if (lstItems != null) {
                    if (lstItems.IsActive == true) {
                        cc.execSQL("Update  MRegistration Set  IsActive=" + ((lstItems.IsActive == true) ? 1 : 0) + " ");
                        ((HomeActivity) getActivity()).UpdateSetting();
                    } else {
                        ShowMessage("Please contact to administrator");
                    }
                }
            }

        }


        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Processing Request...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }
    }

}
