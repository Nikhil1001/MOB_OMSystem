package entity;

/**
 * Created by admin on 8/19/2017.
 */

public class UCLoginDetails {

    public   Long UserID;
    public Long PostType;
    public Long InspectorNo;

    public Long getUserID() {
        return UserID;
    }

    public void setUserID(Long userID) {
        UserID = userID;
    }

    public Long getPostType() {
        return PostType;
    }

    public void setPostType(Long postType) {
        PostType = postType;
    }

    public Long getInspectorNo() {
        return InspectorNo;
    }

    public void setInspectorNo(Long inspectorNo) {
        InspectorNo = inspectorNo;
    }
}
