package entity;

/**
 * Created by admin on 9/28/2017.
 */

public class UCStockItems {

    public long ItemNo;
    public String BrandName;
    public String ItemName;
    public String LangFullDesc;
    public double MRP;
    public double Rate;
    public String Uom;
    public String Barcode;
    public String ItemNameDisp;
    public String HSNCode;
    public double TaxRate;
    public double IGSTPer;
    public double CGSTPer;
    public double SGSTPer;
    public double CessPer;

}
