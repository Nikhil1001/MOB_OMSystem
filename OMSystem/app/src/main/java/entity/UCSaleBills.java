package entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 9/28/2017.
 */

public class UCSaleBills {

    public class UCSaleBill
    {
        public UCTVoucherEntry tvoucherentry;
        public List<UCTStock> tstock= new ArrayList<>();
        public List<UCTVoucherRefDetails> tVoucherrefdetails =new ArrayList<>();

    }

    public class UCSaleBillsList
    {
        public List<UCSaleBill> salesBills = new ArrayList<>();
    }

    public class UCTVoucherEntry {
        public long PkVoucherNo;
        public long VoucherTypeCode;
        public long VoucherUserNo;
        public String VoucherDate;
        public String VoucherTime;
        public String Narration="";
        public String Reference="";
        public long CompanyNo;
        public double BilledAmount;
        public String Remark="";
        public long PayTypeNo;
        public long RateTypeNo;
        public long TaxTypeNo;
        public String VoucherStatus="";
        public long UserID;
        public String ModifiedBy="";
        public long OrderType;
        public double ReturnAmount;
        public double Visibility;
        public double DiscPercent;
        public double DiscAmt;
        public double ChargAmount;
        public String StatusNo;
        public String MixMode="";
        public double HamaliRs;
        public long MobilePkSrNo;
        public long MobileBillNo;
        public long MobileUserNo;
        public String LedgerName="";
        public long LedgerNo;
        public Boolean IsCancel;
    }

    public class UCTStock {
        public long PkvouchertrnNo;
        public long FkvoucherNo;
        public String Barcode;
        public String Itemname;
        public String LangitemName;
        public long ItemNo;
        public double Qty;
        public double Rate;
        public double Mrp;
        public String UomName;
        public double Amount;
        public double NetRate;
        public double NetAmount;
        public double Taxpercentage;
        public double TaxAmount;
        public double DiscAmount;
        public String HSNCode;
        public double IGSTPer;
        public double IGSTAmt;
        public double CGSTPer;
        public double CGSTAmt;
        public double SGSTPer;
        public double SGSTAmt;
        public double CESSPer;
        public double CESSAmt;
        public long TempItemNo;


    }

    public class UCTVoucherRefDetails {
        public long PkSrNo;
        public long PkreftrnNo;
        public long FkvoucherNo;
        public long LedgerNo;
        public long Refno;
        public long TypeofRef;
        public double Amount;
        public long DiscAmount;
        public double Balamount;
        public String PayDate;
        public String PayType;
        public String CardNo;
        public String CheqDate;
        public String Remark;
    }

}