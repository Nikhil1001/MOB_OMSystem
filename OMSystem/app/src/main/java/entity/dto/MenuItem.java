package entity.dto;

import android.graphics.Bitmap;

/**
 * Created by admin on 12/8/2017.
 */

public class MenuItem {

    private Bitmap MenuImage;
    private String MenuTitel;

    public MenuItem(Bitmap MenuImage,String MenuTitel)
    {
        setMenuImage(MenuImage);
        setMenuTitel(MenuTitel);
    }

    public Bitmap getMenuImage() {
        return MenuImage;
    }

    public void setMenuImage(Bitmap menuImage) {
        MenuImage = menuImage;
    }

    public String getMenuTitel() {
        return MenuTitel;
    }

    public void setMenuTitel(String menuTitel) {
        MenuTitel = menuTitel;
    }
}
