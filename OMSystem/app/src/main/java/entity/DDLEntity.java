package entity;

public class DDLEntity {

	public DDLEntity() {
	}

	public DDLEntity(long ID, String name) {
		this.ID = ID;
		this.Name = name;
	}

	long ID;
	String Name;

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

}
