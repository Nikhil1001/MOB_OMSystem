package entity;

/**
 * Created by admin on 10/2/2017.
 */

public class Item implements Cloneable {

    private String ItemName, Barcode,BrandName,ItemNameDisp,UOM;
    private double Mrp,TaxRate,IGSTPer,CGSTPer,SGSTPer,CessPer,Rate,Qty,DiscPer,DiscAmt,NetRate,Amount ;
    private long ItemNo,PkSrNo;


    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getBarcode() {
        return Barcode;
    }

    public void setBarcode(String barcode) {
        Barcode = barcode;
    }

    public double getMrp() {
        return Mrp;
    }

    public void setMrp(double mrp) {
        Mrp = mrp;
    }

    public long getItemNo() {
        return ItemNo;
    }

    public void setItemNo(long itemNo) {
        ItemNo = itemNo;
    }

    public double getTaxRate() {
        return TaxRate;
    }

    public void setTaxRate(double taxRate) {
        TaxRate = taxRate;
    }

    public double getIGSTPer() {
        return IGSTPer;
    }

    public void setIGSTPer(double IGSTPer) {
        this.IGSTPer = IGSTPer;
    }

    public double getCGSTPer() {
        return CGSTPer;
    }

    public void setCGSTPer(double CGSTPer) {
        this.CGSTPer = CGSTPer;
    }

    public double getSGSTPer() {
        return SGSTPer;
    }

    public void setSGSTPer(double SGSTPer) {
        this.SGSTPer = SGSTPer;
    }

    public double getCessPer() {
        return CessPer;
    }

    public void setCessPer(double cessPer) {
        CessPer = cessPer;
    }

    public long getPkSrNo() {
        return PkSrNo;
    }

    public void setPkSrNo(long pkSrNo) {
        PkSrNo = pkSrNo;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public String getItemNameDisp() {
        return ItemNameDisp;
    }

    public void setItemNameDisp(String itemNameDisp) {
        ItemNameDisp = itemNameDisp;
    }

    public String getUOM() {
        return UOM;
    }

    public void setUOM(String UOM) {
        this.UOM = UOM;
    }

    public double getRate() {
        return Rate;
    }

    public void setRate(double rate) {
        Rate = rate;
    }


    public double getQty() {
        return Qty;
    }

    public void setQty(double qty) {
        Qty = qty;
    }

    public double getDiscPer() {
        return DiscPer;
    }

    public void setDiscPer(double discPer) {
        DiscPer = discPer;
    }

    public double getDiscAmt() {
        return DiscAmt;
    }

    public void setDiscAmt(double discAmt) {
        DiscAmt = discAmt;
    }

    public double getNetRate() {
        return NetRate;
    }

    public void setNetRate(double netRate) {
        NetRate = netRate;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }
}
